use std::time::Duration;

use lazy_static::lazy_static;

use crate::{
    ui::{color::Color, graphics::Font},
    utils::geometry::{
        GridConnection, ScreenPoint, ScreenRect, ScreenSideOffsets, ScreenSize, WorldVec,
    },
};
// ***********************************************************************************
//                                        UI
// ***********************************************************************************
pub const SQUARE_CELL_SIDE: f64 = 48.0;

pub const MAP_CELL_SIZE: ScreenSize = ScreenSize::new(SQUARE_CELL_SIDE, SQUARE_CELL_SIDE);
pub const TEXT_CELL_SIZE: ScreenSize = ScreenSize::new(SQUARE_CELL_SIDE / 2.0, SQUARE_CELL_SIDE);

pub const DEFAULT_COLOR: Color = Color::WHITE;
pub const BACKGROUND_COLOR: Color = Color::grey(10);
pub const HIGHLIGHTED_BACKGROUND_COLOR: Color = Color::grey(50);

pub const CANVAS_SIZE: ScreenSize =
    ScreenSize::new(MAP_CELL_SIZE.width * 40.0, SQUARE_CELL_SIDE * 28.0);
pub const CANVAS_RECT: ScreenRect = ScreenRect::new(ScreenPoint::new(0.0, 0.0), CANVAS_SIZE);

pub const PANEL_LINE_THICKNESS: f64 = 0.2 * SQUARE_CELL_SIDE;
pub const CURSOR_LINE_THICKNESS: f64 = 0.15 * SQUARE_CELL_SIDE;
pub const PERIMETER_LINE_THICKNESS: f64 = 0.15 * SQUARE_CELL_SIDE;
pub const ANSWER_SELECTION_LINE_THICKNESS: f64 = 0.1 * SQUARE_CELL_SIDE;

pub const CHAR_Y_OFFSET_RATIO: f64 = 0.79;
pub const FONT_SIZE_RATIO: f64 = 2.0 / 3.0;
lazy_static! {
    pub static ref MAP_FONT: Font = Font::new("Simple_Mood", MAP_CELL_SIZE);
    pub static ref TEXT_FONT: Font = Font::new("FiraCode", TEXT_CELL_SIZE);
    static ref MAP_FONT_2X2: Font = Font::new("Simple_Mood", MAP_CELL_SIZE * 2.0);
    static ref MAP_FONT_3X3: Font = Font::new("Simple_Mood", MAP_CELL_SIZE * 3.0);
}
pub fn map_font_for_size(size: i32) -> &'static Font {
    match size {
        1 => &MAP_FONT,
        2 => &MAP_FONT_2X2,
        3 => &MAP_FONT_3X3,
        _ => panic!("I don't have a font for this size: {size}"),
    }
}

pub const MAP_PANEL_SIZE: ScreenSize = {
    let side = 19.0;
    let width = side;
    let height = side;
    ScreenSize::new(width * MAP_CELL_SIZE.width, height * MAP_CELL_SIZE.height)
};

pub const POPUP_MARGIN: f64 = CANVAS_SIZE.width * 0.1;
pub const POPUP_ANSWER_MARGIN: ScreenSideOffsets = {
    let vertical_offset = TEXT_CELL_SIZE.height / 2.0;
    let horizontal_offset = -vertical_offset;
    ScreenSideOffsets::new(
        vertical_offset,
        horizontal_offset,
        vertical_offset,
        horizontal_offset,
    )
    // ScreenSideOffsets::new(0.0, 0.0, 0.0, 0.0)
};

pub const FLOATING_TEXT_STROKE_WIDTH: f64 = 8.0;
// ***********************************************************************************
//                                   GAMEPLAY
// ***********************************************************************************

pub const MOVEMENT_TYPE: GridConnection = GridConnection::Eight;
pub const FOV_ACTIVATED: bool = false;
pub const PLAYER_VISION_RANGE: i32 = 10;

pub const ORTHOGAL_MOVE_COST: i32 = 100;
pub const DIAGONAL_MOVE_COST: i32 = 141;

// ***********************************************************************************
//                                  ANIMATIONS
// ***********************************************************************************
pub const HIT_MOVE_RATIO: f64 = 0.3;
pub const MOVE_DURATION: Duration = Duration::from_millis(150);
pub const HIT_DURATION: Duration = Duration::from_millis(150);

pub const CHAR_FLASH_DURATION: Duration = Duration::from_millis(100);

pub const SCREEN_FADE_DURATION: Duration = Duration::from_millis(500);

pub const FLOATING_TEXT_SPEED: WorldVec = WorldVec::new(0.0, -3.0);
pub const FLOATING_TEXT_LIFETIME: Duration = Duration::from_millis(500);

// ***********************************************************************************
//                                     COLOR
// ***********************************************************************************

pub const HP_LOST_TEXT_COLOR: Color = Color::RED;

pub const HP_LOST_FLASH_COLOR: Color = Color::RED;

pub const FLOATING_TEXT_STROKE_COLOR: Color = Color::WHITE;

// ***********************************************************************************
//                                    DEBUG
// ***********************************************************************************
pub const DEBUG_ANIMATIONS: bool = false;
pub const DEBUG_INPUT: bool = false;
