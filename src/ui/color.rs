use wasm_bindgen::JsValue;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Color {
    r: u8,
    g: u8,
    b: u8,
}

impl Color {
    pub const WHITE: Self = Self::grey(255);
    pub const BLACK: Self = Self::grey(0);
    pub const DARK_GREY: Self = Self::grey(100);

    pub const BLUE: Self = Self::rgb(0, 0, 255);
    pub const RED: Self = Self::rgb(255, 0, 0);

    pub const fn rgb(r: u8, g: u8, b: u8) -> Self {
        Self { r, g, b }
    }

    pub const fn grey(tint: u8) -> Self {
        Self::rgb(tint, tint, tint)
    }

    pub fn to_hex(&self) -> String {
        format!("#{:02x}{:02x}{:02x}", self.r, self.g, self.b)
    }

    pub fn to_js_value(&self) -> JsValue {
        JsValue::from_str(&self.to_hex())
    }
}
