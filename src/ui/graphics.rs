use std::fmt::Display;

use wasm_bindgen::JsValue;
use web_sys::{CanvasGradient, CanvasRenderingContext2d, HtmlImageElement};

use crate::{
    console_warn,
    constants::{
        CHAR_Y_OFFSET_RATIO, DEFAULT_COLOR, FONT_SIZE_RATIO, MAP_FONT, PERIMETER_LINE_THICKNESS,
        SQUARE_CELL_SIDE, TEXT_FONT,
    },
    js_bindings::Array,
    utils::geometry::{
        Align, RectExtra, ScreenPoint, ScreenRect, ScreenSize, ScreenVec, SizeExtra,
        SpriteSheetRect,
    },
};

use super::{
    color::Color,
    colored_text::{ColoredText, Data},
};

#[derive(Clone, Debug, PartialEq)]
pub struct Font {
    pub name: &'static str,
    pub char_size: ScreenSize,
    pub char_offset: ScreenVec,
}

impl Font {
    pub const fn new(name: &'static str, char_size: ScreenSize) -> Self {
        let char_offset = ScreenVec::new(
            char_size.width / 2.0,
            char_size.height * CHAR_Y_OFFSET_RATIO,
        );

        Self {
            name,
            char_size,
            char_offset,
        }
    }
}

impl Display for Font {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let font_pt = (self.char_size.height * FONT_SIZE_RATIO).floor() as usize;
        let name_with_size = format!("{font_pt}pt {}", self.name);

        name_with_size.fmt(f)
    }
}

pub struct Graphics {
    ctxt: CanvasRenderingContext2d,
    font: &'static Font,
    fill_style: Option<FillStyle>,
    stroke_color: Option<Color>,
    dash_style: Option<DashStyle>,
    corner_style: Option<CornerStyle>,
}

impl Graphics {
    pub fn new(ctxt: CanvasRenderingContext2d) -> Self {
        ctxt.set_text_align("center");

        let mut g = Self {
            ctxt,
            font: &MAP_FONT,
            fill_style: None,
            stroke_color: None,
            dash_style: None,
            corner_style: None,
        };
        g.set_font(&TEXT_FONT);
        g.set_fill_color(DEFAULT_COLOR);
        g.set_stroke_color(DEFAULT_COLOR);
        g.set_dash_style(DashStyle::Solid);
        g.set_corner_style(CornerStyle::Square);

        g
    }

    pub fn set_font(&mut self, font: &'static Font) {
        if self.font != font {
            self.font = font;
            self.ctxt.set_font(font.to_string().as_str());
        }
    }

    pub fn set_fill_color(&mut self, color: Color) {
        self.set_fill_style(FillStyle::Color(color));
    }

    fn set_fill_style(&mut self, fill_style: FillStyle) {
        if !self.fill_style.contains(&fill_style) {
            match &fill_style {
                FillStyle::Color(color) => self.ctxt.set_fill_style(&color.to_js_value()),
                FillStyle::Gradient(gradient) => self.ctxt.set_fill_style(&gradient),
            }
            self.fill_style = Some(fill_style);
        }
    }

    pub fn set_stroke_color(&mut self, color: Color) {
        if !self.stroke_color.contains(&color) {
            self.stroke_color = Some(color);
            self.ctxt.set_stroke_style(&color.to_js_value());
        }
    }

    fn set_mode_color(&mut self, color: Color, mode: Mode) {
        match mode {
            Mode::Stroke => self.set_stroke_color(color),
            Mode::Fill => self.set_fill_color(color),
        }
    }

    pub fn set_line_width(&mut self, width: f64) {
        self.ctxt.set_line_width(width);
    }

    pub fn set_dash_style(&mut self, dash_style: DashStyle) {
        if !self.dash_style.contains(&dash_style) {
            self.dash_style = Some(dash_style);
            self.ctxt
                .set_line_dash(&dash_style.to_js_value())
                .expect("Failed to set dash style.");

            if let DashStyle::Dashed { offset, .. } = dash_style {
                self.ctxt.set_line_dash_offset(offset);
            }
        }
    }

    pub fn set_corner_style(&mut self, corner_style: CornerStyle) {
        if !self.corner_style.contains(&corner_style) {
            self.corner_style = Some(corner_style);
            self.ctxt.set_line_cap(corner_style.line_cap());
            self.ctxt.set_line_join(corner_style.line_join());
        }
    }

    pub fn set_opacity(&mut self, opacity: f64) {
        self.ctxt.set_global_alpha(opacity.clamp(0.0, 1.0));
    }

    /// @return the bounding-box of the text
    pub fn write_text_in_rect(
        &mut self,
        text: &ColoredText,
        rect: ScreenRect,
        align: Align,
    ) -> ScreenRect {
        let anchor = rect.anchor(align);
        self.write_text(text, anchor, align, Some(rect.width()))
    }

    /// @return the bounding-box of the text
    pub fn write_text(
        &mut self,
        text: &ColoredText,
        anchor: ScreenPoint,
        align: Align,
        max_width: Option<f64>,
    ) -> ScreenRect {
        self.write_or_stroke_text(text, anchor, align, max_width, Mode::Fill)
    }

    /// @return the bounding-box of the text. It may
    pub fn stroke_text(
        &mut self,
        text: &ColoredText,
        anchor: ScreenPoint,
        align: Align,
        max_width: Option<f64>,
    ) -> ScreenRect {
        self.write_or_stroke_text(text, anchor, align, max_width, Mode::Stroke)
    }

    fn write_or_stroke_text(
        &mut self,
        text: &ColoredText,
        anchor: ScreenPoint,
        align: Align,
        max_width: Option<f64>,
        mode: Mode,
    ) -> ScreenRect {
        let wrapped_text = max_width.map(|max_width| {
            let line_width: usize = (max_width / self.font.char_size.width).floor() as usize;
            text.wrapped(line_width)
        });
        let text = wrapped_text.as_ref().unwrap_or(text);
        let needed_space = text.needed_space_screen(self.font.char_size);
        let bounding_rect = ScreenRect::from_anchor(anchor, needed_space, align);

        let mut y = bounding_rect.min_y();

        for line in text.lines() {
            let needed_space = ScreenSize::new(
                self.font.char_size.width * line.char_count() as f64,
                self.font.char_size.height,
            );
            let x = needed_space.aligned_inside(&bounding_rect, align).min_x();

            self.write_or_stroke_text_line(&line, ScreenPoint::new(x, y), mode);

            y += self.font.char_size.height;
        }

        bounding_rect
    }

    fn write_or_stroke_text_line(&mut self, line: &ColoredText, top_left: ScreenPoint, mode: Mode) {
        let mut pos = top_left;
        let mut current_color = DEFAULT_COLOR;
        let mut color_stacks = Vec::new();
        self.set_fill_color(current_color);

        for d in line.iter() {
            match *d {
                Data::SetFrontColor(color) => {
                    self.set_mode_color(color, mode);
                    color_stacks.push(current_color);
                    current_color = color;
                }
                Data::ResetFrontColor => {
                    let new_color = color_stacks.pop().unwrap_or(DEFAULT_COLOR);
                    self.set_mode_color(new_color, mode);
                }
                Data::Char(c) => {
                    if c == '\n' {
                        console_warn!("Newline character while writing a line! Ignoring.")
                    } else {
                        self.write_or_stroke_char(c, pos, mode);
                        pos.x += self.font.char_size.width;
                    }
                }
            };
        }
    }

    pub fn fill_polyline(&mut self, points: &[ScreenPoint], color: Color) {
        if points.is_empty() {
            return;
        }

        self.path_polyline(points);

        self.set_fill_color(color);
        self.ctxt.fill();
    }

    pub fn stroke_polyline(&mut self, points: &[ScreenPoint], color: Color, line_width: f64) {
        if points.is_empty() {
            return;
        }

        self.path_polyline(points);

        self.set_line_width(line_width);
        self.set_stroke_color(color);
        self.ctxt.stroke();
    }

    pub fn stroke_line(
        &mut self,
        from: ScreenPoint,
        to: ScreenPoint,
        color: Color,
        line_width: f64,
    ) {
        self.stroke_line_batch(std::iter::once((from, to)), color, line_width)
    }

    pub fn stroke_line_batch<I>(&mut self, lines: I, color: Color, line_width: f64)
    where
        I: IntoIterator<Item = (ScreenPoint, ScreenPoint)>,
    {
        self.set_line_width(line_width);
        self.set_stroke_color(color);

        self.ctxt.begin_path();
        for (a, b) in lines {
            self.ctxt.move_to(a.x, a.y);
            self.ctxt.line_to(b.x, b.y);
        }
        self.ctxt.stroke();
    }

    pub fn fill_rect(&mut self, rect: ScreenRect, color: Color) {
        self.set_fill_color(color);
        self.ctxt
            .fill_rect(rect.min_x(), rect.min_y(), rect.width(), rect.height());
    }

    pub fn stroke_rect(&mut self, rect: ScreenRect, color: Color, line_width: f64) {
        self.set_line_width(line_width);
        self.set_stroke_color(color);
        self.ctxt
            .stroke_rect(rect.min_x(), rect.min_y(), rect.width(), rect.height());
    }

    fn path_polyline(&mut self, points: &[ScreenPoint]) {
        self.ctxt.begin_path();
        self.ctxt.move_to(points[0].x, points[0].y);

        for pt in points {
            self.ctxt.line_to(pt.x, pt.y);
        }
    }

    pub fn write_char(&mut self, c: char, pos: ScreenPoint) {
        self.write_or_stroke_char(c, pos, Mode::Fill);
    }

    fn write_or_stroke_char(&mut self, c: char, pos: ScreenPoint, mode: Mode) {
        if !c.is_whitespace() {
            let pos = pos + self.font.char_offset;
            (match mode {
                Mode::Stroke => self.ctxt.stroke_text(&c.to_string(), pos.x, pos.y),
                Mode::Fill => self.ctxt.fill_text(&c.to_string(), pos.x, pos.y),
            })
            .expect("error while drawing char");
        }
    }

    pub fn write_multicolor_char(&mut self, c: char, pos: ScreenPoint, colors: &[Color]) {
        match colors.len() {
            0 => {
                self.write_char(c, pos);
            }
            1 => {
                self.set_fill_color(colors[0]);
                self.write_char(c, pos);
            }
            l => {
                let step = 1.0 / (l as f32);
                let rect = ScreenRect::new(pos, self.font.char_size)
                    .inner_rect_same(self.font.char_size.height * 0.1);
                let gradient = self.ctxt.create_linear_gradient(
                    rect.min_x(),
                    rect.min_y(),
                    rect.max_x(),
                    rect.max_y(),
                );

                for i in 1..l {
                    let offset = step * (i as f32);
                    gradient
                        .add_color_stop(offset - 0.001, &colors[i - 1].to_hex())
                        .expect("Error on gradient.add_color_stop()");
                    gradient
                        .add_color_stop(offset, &colors[i].to_hex())
                        .expect("Error on gradient.add_color_stop()");
                }

                self.set_fill_style(FillStyle::Gradient(gradient));
                self.write_char(c, pos);
            }
        }
    }

    pub fn draw_sprite(&mut self, sprite: &SpriteSpec, rect: ScreenRect) {
        let image = HtmlImageElement::new().unwrap();
        image.set_src(sprite.image_path);
        self.ctxt
            .draw_image_with_html_image_element_and_sw_and_sh_and_dx_and_dy_and_dw_and_dh(
                &image,
                sprite.rect.min_x(),
                sprite.rect.min_y(),
                sprite.rect.width(),
                sprite.rect.height(),
                rect.min_x(),
                rect.min_y(),
                rect.width(),
                rect.height(),
            )
            .unwrap();
    }
}

#[derive(PartialEq, Debug, Clone, Copy)]
pub enum DashStyle {
    Solid,
    Dashed { dash: f64, gap: f64, offset: f64 },
}

impl DashStyle {
    pub const SMALL: Self =
        Self::dashed_equal(SQUARE_CELL_SIDE / 6.0, PERIMETER_LINE_THICKNESS, false);
    pub const SMALL_INV: Self =
        Self::dashed_equal(SQUARE_CELL_SIDE / 6.0, PERIMETER_LINE_THICKNESS, true);
    pub const MEDIUM: Self =
        Self::dashed_equal(SQUARE_CELL_SIDE / 4.0, PERIMETER_LINE_THICKNESS, false);
    pub const MEDIUM_INV: Self =
        Self::dashed_equal(SQUARE_CELL_SIDE / 4.0, PERIMETER_LINE_THICKNESS, true);
    pub const CURSOR_LIKE: Self =
        Self::dashed_equal(SQUARE_CELL_SIDE / 2.0, PERIMETER_LINE_THICKNESS, false);
    pub const DIGITAL_LIKE: Self =
        Self::dashed_equal(SQUARE_CELL_SIDE / 2.0, PERIMETER_LINE_THICKNESS, true);
    /// The gap and the dash are the same length,
    /// Taking into account the line thickness.
    const fn dashed_equal(dash_length: f64, line_thickness: f64, gap_in_corner: bool) -> Self {
        let dash = dash_length - line_thickness;
        let gap = dash_length + line_thickness;
        let offset = if gap_in_corner {
            dash + gap / 2.0
        } else {
            dash / 2.0
        };

        Self::Dashed { dash, gap, offset }
    }

    fn to_js_value(&self) -> JsValue {
        let array = match self {
            DashStyle::Solid => Array::new(),
            DashStyle::Dashed { dash, gap, .. } => Array::of_two_f64(*dash, *gap),
        };
        array.into()
    }

    pub fn with_offset(&self, add_offset: f64) -> Self {
        let mut result = self.clone();

        if let DashStyle::Dashed { ref mut offset, .. } = result {
            *offset += add_offset;
        }

        result
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum CornerStyle {
    Round,
    Square,
}

impl CornerStyle {
    fn line_join(&self) -> &'static str {
        match self {
            CornerStyle::Round => "round",
            CornerStyle::Square => "miter",
        }
    }

    fn line_cap(&self) -> &'static str {
        match self {
            CornerStyle::Round => "round",
            CornerStyle::Square => "square",
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
enum Mode {
    Stroke,
    Fill,
}

#[derive(Clone, Debug, PartialEq, Eq)]
enum FillStyle {
    Color(Color),
    Gradient(CanvasGradient),
}

#[derive(Clone, Debug, PartialEq)]
pub struct SpriteSpec {
    pub image_path: &'static str,
    pub rect: SpriteSheetRect,
}
