use std::{
    collections::VecDeque,
    fmt::Display,
    ops::{Add, AddAssign},
};

use crate::utils::geometry::{PanelPos, PanelSize, ScreenSize};

use super::color::Color;

#[derive(Clone, Debug)]
pub struct ColoredText {
    data: VecDeque<Data>,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub(super) enum Data {
    Char(char),
    SetFrontColor(Color),
    ResetFrontColor,
}

impl Data {
    const NEWLINE: Self = Self::Char('\n');
    const SPACE: Self = Self::Char(' ');

    #[inline]
    fn is_char(&self) -> bool {
        match self {
            Self::Char(_) => true,
            _ => false,
        }
    }
}

impl ColoredText {
    fn from_str(text: &str) -> Self {
        let data = text.chars().map(|c| Data::Char(c)).collect();
        Self { data }
    }

    fn empty() -> Self {
        Self {
            data: VecDeque::new(),
        }
    }

    fn chars(&self) -> impl Iterator<Item = char> + '_ {
        self.data.iter().filter_map(|d| {
            if let Data::Char(c) = d {
                Some(*c)
            } else {
                None
            }
        })
    }

    pub fn char_count(&self) -> usize {
        self.chars().count()
    }

    pub fn line_count(&self) -> usize {
        self.data.iter().filter(|d| **d == Data::NEWLINE).count() + 1
    }

    pub fn wrapped_screen(&self, max_width: f64, char_size: ScreenSize) -> ColoredText {
        let line_length = (max_width / char_size.width).floor() as usize;
        self.wrapped(line_length)
    }

    /// Add newlines (and potentially remove spaces) to wrap
    /// the text inside the line length.
    pub fn wrapped(&self, line_length: usize) -> ColoredText {
        let mut output_data = VecDeque::new();

        let mut x = 0;

        for &d in &self.data {
            output_data.push_back(d);
            if d == Data::NEWLINE {
                x = 0;
            } else if d.is_char() {
                x += 1;
                if x > line_length {
                    // lets find a space where to cut the line.
                    if let Some(space) = output_data.iter_mut().rev().find(|d| **d == Data::SPACE) {
                        *space = Data::NEWLINE;
                        x = output_data
                            .iter()
                            .rev()
                            .take_while(|d| **d != Data::NEWLINE)
                            .filter(|d| d.is_char())
                            .count();
                    } else {
                        // Cut the word, then.
                        let c = output_data.pop_back().unwrap();
                        output_data.push_back(Data::NEWLINE);
                        output_data.push_back(c);
                        x = 1;
                    }
                }
            }
        }

        Self { data: output_data }
    }

    pub(super) fn iter(&self) -> impl Iterator<Item = &Data> {
        self.data.iter()
    }

    /// @return separated lines without newline at the end
    pub fn lines(&self) -> Vec<Self> {
        self.iter()
            .fold(
                (vec![Self::empty()], Vec::new()),
                |(mut acc, mut color_stack), &d| {
                    if d == Data::NEWLINE {
                        let current_line = acc.last_mut().unwrap();
                        let mut new_line = Self::empty();
                        for &c in color_stack.iter() {
                            current_line.data.push_back(Data::ResetFrontColor);
                            new_line.data.push_back(Data::SetFrontColor(c));
                        }
                        acc.push(new_line);
                    } else {
                        acc.last_mut().unwrap().data.push_back(d);

                        match d {
                            Data::Char(_) => (),
                            Data::SetFrontColor(color) => {
                                color_stack.push(color);
                            }
                            Data::ResetFrontColor => {
                                color_stack.pop();
                            }
                        };
                    }

                    (acc, color_stack)
                },
            )
            .0
    }

    pub fn needed_space_grid(&self) -> PanelSize {
        let char_size = PanelSize::splat(1);
        let top_left = PanelPos::origin();
        let mut pos = top_left;
        let mut bottom_right = pos;

        for &d in self.iter() {
            if d == Data::NEWLINE {
                pos.x = 0;
                pos.y += char_size.height;
            } else if d.is_char() {
                bottom_right = bottom_right.max(pos + char_size);
                pos.x += char_size.width;
            }
        }

        (bottom_right - top_left).into()
    }

    pub fn needed_space_screen(&self, char_size: ScreenSize) -> ScreenSize {
        let grid_size: ScreenSize = self.needed_space_grid().cast();

        ScreenSize::new(
            grid_size.width * char_size.width,
            grid_size.height * char_size.height,
        )
    }

    pub fn join(iter: impl IntoIterator<Item = Self>, sep: Self) -> Self {
        let mut builder = Self::empty();
        let mut is_first = true;
        for text in iter {
            if is_first {
                is_first = false;
            } else {
                builder += sep.clone();
            }
            builder += text;
        }
        builder
    }

    pub fn with_overriden_color(&self, color: Color) -> Self {
        self.to_string().colored(color)
    }
}

impl From<&str> for ColoredText {
    fn from(value: &str) -> Self {
        Self::from_str(value)
    }
}

impl From<String> for ColoredText {
    fn from(value: String) -> Self {
        Self::from_str(&value)
    }
}

pub trait Colorable {
    fn colored(self, color: Color) -> ColoredText;
}

impl Colorable for &String {
    fn colored(self, color: Color) -> ColoredText {
        self.as_str().colored(color)
    }
}

impl Colorable for &str {
    fn colored(self, color: Color) -> ColoredText {
        ColoredText::from_str(self).colored(color)
    }
}

impl Colorable for ColoredText {
    fn colored(mut self, color: Color) -> ColoredText {
        self.data.push_front(Data::SetFrontColor(color));
        self.data.push_back(Data::ResetFrontColor);

        self
    }
}

impl Add<ColoredText> for ColoredText {
    type Output = ColoredText;

    fn add(mut self, rhs: ColoredText) -> Self::Output {
        self += rhs;
        self
    }
}

impl Add<&str> for ColoredText {
    type Output = ColoredText;

    fn add(mut self, rhs: &str) -> Self::Output {
        self += rhs;
        self
    }
}

impl Add<String> for ColoredText {
    type Output = ColoredText;

    fn add(mut self, rhs: String) -> Self::Output {
        self += rhs;
        self
    }
}

impl AddAssign<ColoredText> for ColoredText {
    fn add_assign(&mut self, mut rhs: ColoredText) {
        self.data.append(&mut rhs.data)
    }
}

impl AddAssign<&str> for ColoredText {
    fn add_assign(&mut self, rhs: &str) {
        *self += Self::from_str(rhs);
    }
}

impl AddAssign<String> for ColoredText {
    fn add_assign(&mut self, rhs: String) {
        *self += Self::from_str(rhs.as_str());
    }
}

impl Add<ColoredText> for &str {
    type Output = ColoredText;

    fn add(self, rhs: ColoredText) -> Self::Output {
        ColoredText::from_str(self) + rhs
    }
}

impl Display for ColoredText {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.chars().collect::<String>().fmt(f)
    }
}

pub trait ColoredTextIter {
    fn join(self, sep: ColoredText) -> ColoredText;
}

impl<I> ColoredTextIter for I
where
    I: Iterator<Item = ColoredText>,
{
    fn join(self, sep: ColoredText) -> ColoredText {
        ColoredText::join(self, sep)
    }
}

pub trait DisplayColored {
    fn display_colored(&self) -> ColoredText;
}
