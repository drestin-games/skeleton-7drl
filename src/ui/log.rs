use std::collections::{vec_deque, VecDeque};

use crate::{console_log, ui::colored_text::ColoredText};

#[derive(Debug)]
pub struct GameLogs {
    logs: VecDeque<ColoredText>,
    capacity: usize,
}

impl GameLogs {
    pub fn new() -> Self {
        Self::with_capacity(20)
    }

    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            logs: VecDeque::with_capacity(capacity),
            capacity,
        }
    }

    pub fn push(&mut self, log_str: ColoredText) {
        if self.logs.len() >= self.capacity {
            self.logs.pop_back();
        }

        console_log!("{log_str}");
        self.logs.push_front(log_str);
    }

    pub fn clear(&mut self) {
        self.logs.clear();
    }

    pub fn iter(&self) -> IterLogs<'_> {
        self.logs.iter()
    }
}

impl Default for GameLogs {
    fn default() -> Self {
        Self::new()
    }
}

pub type IterLogs<'a> = vec_deque::Iter<'a, ColoredText>;
impl<'a> IntoIterator for &'a GameLogs {
    type Item = &'a ColoredText;

    type IntoIter = IterLogs<'a>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}
