use crate::{
    ui::{
        colored_text::ColoredText,
        graphics::{Font, Graphics},
    },
    utils::geometry::{Align, HAlign, ScreenBox, ScreenRect, ScreenSize, SizeExtra, VAlign},
};

#[derive(Debug)]
enum InstructionType {
    NewLine,
    Text(ColoredText),
    ShortFloating(ColoredText, HAlign),
}
#[derive(Debug)]
struct WriteInstr {
    instr: InstructionType,
    font: &'static Font,
}

impl WriteInstr {
    fn new(instr: InstructionType, font: &'static Font) -> Self {
        Self { instr, font }
    }

    fn used_height(&self) -> f64 {
        match &self.instr {
            NewLine => self.font.char_size.height,
            Text(text) => self.font.char_size.height * text.line_count() as f64,
            ShortFloating(_, _) => 0.0,
        }
    }
}
#[derive(Debug)]
pub struct PanelWriter {
    instructions: Vec<WriteInstr>,
    align: Align,
    container_rect: ScreenRect,
    current_font: &'static Font,
    from_bottom: bool,
}

use InstructionType::*;
impl PanelWriter {
    pub fn new(container_rect: ScreenRect, align: Align, font: &'static Font) -> Self {
        Self {
            instructions: Vec::new(),
            container_rect,
            align,
            current_font: font,
            from_bottom: false,
        }
    }

    pub fn from_bottom(mut self) -> Self {
        self.from_bottom = true;
        self
    }

    pub fn newline(&mut self) -> WriterInstructionHandle {
        self.instructions
            .push(WriteInstr::new(NewLine, self.current_font));
        WriterInstructionHandle(self.instructions.len() - 1)
    }

    pub fn writeln(&mut self, text: ColoredText) -> WriterInstructionHandle {
        self.instructions.push(WriteInstr::new(
            Text(text.wrapped_screen(self.container_rect.width(), self.current_font.char_size)),
            self.current_font,
        ));
        WriterInstructionHandle(self.instructions.len() - 1)
    }

    /// This function doesn't update the cursor.
    /// It is intented to be used to override the horizontal aligment to write a small text.
    /// Call newline() afterward to change line.
    pub fn write_short_floating(
        &mut self,
        text: ColoredText,
        h_align: HAlign,
    ) -> WriterInstructionHandle {
        self.instructions.push(WriteInstr::new(
            ShortFloating(text, h_align),
            self.current_font,
        ));
        WriterInstructionHandle(self.instructions.len() - 1)
    }

    pub fn needed_size(&self) -> ScreenSize {
        let needed_height = self.instructions.iter().map(|i| i.used_height()).sum();
        ScreenSize::new(self.container_rect.width(), needed_height)
    }

    pub fn used_rect(&self) -> ScreenRect {
        self.needed_size()
            .aligned_inside(&self.container_rect, self.align)
    }

    pub fn rect_for_instruction(&self, id: WriterInstructionHandle) -> ScreenRect {
        let mut remaining_box: ScreenBox = self.used_rect().to_box2d();
        for instr in self.instructions.iter().take(id.0) {
            self.update_remaining_box(&mut remaining_box, instr);
        }

        let instr = self
            .instructions
            .get(id.0)
            .expect("Instruction id is not valid");

        let height = match &instr.instr {
            NewLine | Text(_) => instr.used_height(),
            ShortFloating(_, _) => instr.font.char_size.height,
        };
        let width = match &instr.instr {
            NewLine | Text(_) => self.container_rect.width(),
            ShortFloating(text, _) => text.needed_space_screen(instr.font.char_size).width,
        };
        let h_align = match instr.instr {
            NewLine | Text(_) => self.align.h_align,
            ShortFloating(_, h_align) => h_align,
        };

        let align = Align::new(self.render_v_align(), h_align);
        ScreenSize::new(width, height).aligned_inside(&remaining_box.to_rect(), align)
    }

    pub fn render(&self, g: &mut Graphics) {
        let v_align = self.render_v_align();
        let mut remaining_box: ScreenBox = self.used_rect().to_box2d();
        for instr in self.instructions.iter() {
            let used_height = instr.used_height();
            if remaining_box.height() < used_height {
                break;
            }
            match &instr.instr {
                NewLine => {} // do nothing
                Text(text) => {
                    g.set_font(instr.font);
                    g.write_text_in_rect(
                        &text,
                        remaining_box.to_rect(),
                        Align::new(v_align, self.align.h_align),
                    );
                }
                ShortFloating(text, h_align) => {
                    g.set_font(instr.font);
                    g.write_text_in_rect(
                        &text,
                        remaining_box.to_rect(),
                        Align::new(v_align, *h_align),
                    );
                }
            }
            self.update_remaining_box(&mut remaining_box, instr);
        }
    }

    fn update_remaining_box(&self, remaining_box: &mut ScreenBox, instr: &WriteInstr) {
        if self.from_bottom {
            remaining_box.max.y -= instr.used_height();
        } else {
            remaining_box.min.y += instr.used_height();
        }
    }

    fn render_v_align(&self) -> VAlign {
        if self.from_bottom {
            VAlign::Bottom
        } else {
            VAlign::Top
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct WriterInstructionHandle(usize);
