use std::collections::HashSet;

use crate::{
    constants::{
        CURSOR_LINE_THICKNESS, FLOATING_TEXT_STROKE_COLOR, FLOATING_TEXT_STROKE_WIDTH,
        MAP_CELL_SIZE, PERIMETER_LINE_THICKNESS,
    },
    ui::{
        color::Color,
        colored_text::ColoredText,
        graphics::{DashStyle, Font, Graphics},
    },
    utils::{
        algorithms::flood_fill::perimeter_edges,
        geometry::{
            Align, CameraTransform, Corner, GridPos, ScreenPoint, ScreenRect, SizeExtra,
            WorldPoint, WorldRect, WorldSize,
        },
    },
};

use super::panel::Panel;

pub struct MapPanel {
    panel: Panel,
    camera_center: WorldPoint,
    zoom: f64,
    camera_transform: CameraTransform,
}

impl MapPanel {
    pub fn new(rect: ScreenRect, camera_center: WorldPoint) -> Self {
        let mut map_panel = Self {
            panel: Panel::from(rect),
            camera_center,
            zoom: MAP_CELL_SIZE.height,
            camera_transform: CameraTransform::default(),
        };
        map_panel.update_camera();
        map_panel
    }

    pub fn screen_to_grid(&self, point: ScreenPoint) -> Option<GridPos> {
        if self.panel.content_rect().contains(point) {
            Some(
                self.camera()
                    .inverse()
                    .unwrap()
                    .transform_point(point)
                    .floor()
                    .cast(),
            )
        } else {
            None
        }
    }

    pub fn grid_to_screen(&self, pos: GridPos) -> ScreenPoint {
        self.world_to_screen(pos.cast())
    }

    pub fn world_to_screen(&self, point: WorldPoint) -> ScreenPoint {
        self.camera().transform_point(point)
    }

    pub fn set_camera_center(&mut self, center: WorldPoint) {
        self.camera_center = center;
        self.update_camera();
    }

    pub fn is_tile_in_panel(&self, pos: GridPos) -> bool {
        let tile_rect = WorldRect::new(pos.cast(), WorldSize::splat(1.0));
        let rendered_rect = self.camera().outer_transformed_rect(&tile_rect);

        self.panel.content_rect().intersects(&rendered_rect)
    }

    pub fn tiles_in_panel(&self) -> impl Iterator<Item = GridPos> {
        let rendered_rect = self.panel.content_rect();
        let world_rect = self
            .camera()
            .inverse()
            .unwrap()
            .outer_transformed_rect(&rendered_rect);

        let min_x = world_rect.min_x().floor() as i32;
        let max_x = world_rect.max_x().ceil() as i32;
        let min_y = world_rect.min_y().floor() as i32;
        let max_y = world_rect.max_y().ceil() as i32;

        (min_y..max_y).flat_map(move |y| (min_x..max_x).map(move |x| (x, y).into()))
    }

    pub fn panel(&self) -> &Panel {
        &self.panel
    }

    fn camera(&self) -> CameraTransform {
        self.camera_transform
    }

    fn update_camera(&mut self) {
        self.camera_transform = CameraTransform::scale(self.zoom, self.zoom)
            .pre_translate(-self.camera_center.to_vector())
            .then_translate(self.panel.center().to_vector())
    }

    pub fn fill_map_tile(&self, g: &mut Graphics, pos: GridPos, color: Color) {
        let rect = ScreenRect::new(self.grid_to_screen(pos), MAP_CELL_SIZE);
        g.fill_rect(rect, color);
    }

    pub fn write_map_char(&self, g: &mut Graphics, c: char, pos: WorldPoint, color: Color) {
        g.set_fill_color(color);
        g.write_char(c, self.world_to_screen(pos));
    }

    pub fn draw_cursor(&self, g: &mut Graphics, pos: WorldPoint) {
        let rect = ScreenRect::new(self.world_to_screen(pos), MAP_CELL_SIZE);
        g.set_dash_style(DashStyle::CURSOR_LIKE);
        g.stroke_rect(rect, Color::WHITE, CURSOR_LINE_THICKNESS);
        g.set_dash_style(DashStyle::Solid);
    }

    pub fn stroke_perimeter_of_area(
        &self,
        g: &mut Graphics,
        area: &HashSet<GridPos>,
        color: Color,
    ) {
        let lines = perimeter_edges(area).into_iter().filter_map(|edge| {
            if self.is_tile_in_panel(edge.tile()) {
                let (Corner(a), Corner(b)) = edge.corners();
                Some((self.grid_to_screen(a), self.grid_to_screen(b)))
            } else {
                None
            }
        });

        g.stroke_line_batch(lines, color, PERIMETER_LINE_THICKNESS);
    }

    pub fn fill_area(&self, g: &mut Graphics, area: &HashSet<GridPos>, color: Color) {
        for &pos in area {
            if self.is_tile_in_panel(pos) {
                self.fill_map_tile(g, pos, color);
            }
        }
    }

    pub fn write_world_text(&self, g: &mut Graphics, text: &WorldText) {
        let pos = self.world_to_screen(text.pos);
        let needed_rect = text
            .text
            .needed_space_screen(text.font.char_size)
            .anchored(pos, text.align);
        if needed_rect.intersects(&self.panel.content_rect()) {
            g.set_font(text.font);
            g.set_line_width(FLOATING_TEXT_STROKE_WIDTH);
            g.stroke_text(
                &text.text.with_overriden_color(FLOATING_TEXT_STROKE_COLOR),
                pos,
                text.align,
                None,
            );
            g.write_text(&text.text, pos, text.align, None);
        }
    }
}

#[derive(Debug, Clone)]
pub struct WorldText {
    pub text: ColoredText,
    pub font: &'static Font,
    pub pos: WorldPoint,
    pub align: Align,
}
