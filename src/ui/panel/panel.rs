use crate::{
    constants::{BACKGROUND_COLOR, DEFAULT_COLOR, PANEL_LINE_THICKNESS, SQUARE_CELL_SIDE},
    ui::{color::Color, graphics::Graphics},
    utils::geometry::{RectExtra, ScreenPoint, ScreenRect, ScreenVec},
};

#[derive(Debug, Clone)]
pub struct Panel {
    rect: ScreenRect,
    margin: f64,
    padding: f64,
    // not added to margin and padding, this field is just for drawing
    border_thickness: f64,
    border_color: Color,
    background_color: Color,
}

impl Panel {
    pub fn new(rect: ScreenRect) -> Self {
        Self {
            rect,
            margin: 0.0,
            padding: SQUARE_CELL_SIDE / 2.0,
            border_thickness: PANEL_LINE_THICKNESS,
            border_color: DEFAULT_COLOR,
            background_color: BACKGROUND_COLOR,
        }
    }

    pub fn with_margin(mut self, margin: f64) -> Self {
        self.margin = margin;
        self
    }

    pub fn render_border(&self, g: &mut Graphics) {
        g.stroke_rect(self.border_rect(), self.border_color, self.border_thickness);
    }

    pub fn render_double_border(&self, g: &mut Graphics) {
        g.stroke_rect(
            self.content_rect().outer_rect_same(1.5),
            self.border_color,
            self.border_thickness,
        );
        self.render_border(g);
    }

    pub fn fill_background(&self, g: &mut Graphics) {
        g.fill_rect(self.border_rect(), self.background_color);
    }

    pub fn clear_padding_and_margin(&self, g: &mut Graphics) {
        g.stroke_rect(
            self.content_rect()
                .outer_rect_same((self.padding + self.margin) / 2.0),
            self.background_color,
            self.padding + self.margin,
        );
    }

    pub fn content_rect(&self) -> ScreenRect {
        self.bounding_rect()
            .inner_rect_same(self.margin + self.padding)
    }

    pub fn border_rect(&self) -> ScreenRect {
        self.bounding_rect().inner_rect_same(self.margin)
    }

    pub fn bounding_rect(&self) -> ScreenRect {
        self.rect
    }

    pub fn content_offset(&self) -> ScreenVec {
        self.content_origin().to_vector()
    }

    pub fn content_origin(&self) -> ScreenPoint {
        self.rect.origin + ScreenVec::splat(self.margin + self.padding)
    }

    pub fn center(&self) -> ScreenPoint {
        self.rect.center()
    }
}

impl From<ScreenRect> for Panel {
    fn from(rect: ScreenRect) -> Self {
        Self::new(rect)
    }
}
