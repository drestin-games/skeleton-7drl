use crate::{
    constants::{
        ANSWER_SELECTION_LINE_THICKNESS, CANVAS_RECT, CANVAS_SIZE, DEFAULT_COLOR,
        HIGHLIGHTED_BACKGROUND_COLOR, POPUP_ANSWER_MARGIN, POPUP_MARGIN, SQUARE_CELL_SIDE,
        TEXT_FONT,
    },
    ui::{
        colored_text::{ColoredText, DisplayColored},
        graphics::Graphics,
    },
    utils::geometry::{Align, Direction, ScreenPoint, ScreenRect, ScreenSize, SizeExtra},
};

use super::{panel::Panel, panel_writer::PanelWriter};

#[derive(Debug)]
pub struct PopupPrompt<A> {
    answers: Vec<(A, ScreenRect)>,
    writer: PanelWriter,
    panel: Panel,
    selected_answer: A,
}

impl<A: DisplayColored + PartialEq + Clone> PopupPrompt<A> {
    pub fn new(question: ColoredText, answers: impl IntoIterator<Item = A>) -> Self {
        let rect = ScreenSize::new(
            CANVAS_SIZE.width - POPUP_MARGIN * 2.0 - SQUARE_CELL_SIDE,
            CANVAS_SIZE.height,
        )
        .aligned_inside(&CANVAS_RECT, Align::MIDDLE_CENTER);
        let mut writer = PanelWriter::new(rect, Align::MIDDLE_CENTER, &TEXT_FONT);
        writer.writeln(question);
        writer.newline();
        writer.newline();
        let answers: Vec<_> = answers
            .into_iter()
            .map(|answer| {
                let handle = writer.writeln(answer.display_colored());
                writer.newline();
                (answer, handle)
            })
            .collect::<Vec<_>>()
            .into_iter()
            .map(|(answer, handle)| {
                let rect = writer
                    .rect_for_instruction(handle)
                    .outer_rect(POPUP_ANSWER_MARGIN);
                (answer, rect)
            })
            .collect();

        let size = ScreenSize::new(
            CANVAS_SIZE.width - POPUP_MARGIN * 2.0,
            writer.needed_size().height + SQUARE_CELL_SIDE,
        );
        let rect = size.aligned_inside(&CANVAS_RECT, Align::MIDDLE_CENTER);
        let panel = Panel::new(rect);

        let selected_answer = answers
            .first()
            .expect("there should be at least one answer")
            .0
            .clone();

        Self {
            answers,
            writer,
            panel,
            selected_answer,
        }
    }

    pub fn select_answer_with_mouse(&mut self, mouse_pos: ScreenPoint) {
        self.answers
            .iter()
            .find(|(_, rect)| rect.contains(mouse_pos))
            .map(|(a, _)| {
                self.selected_answer = a.clone();
            });
    }

    pub fn is_mouse_on_answer(&self, mouse_pos: ScreenPoint) -> bool {
        self.answers
            .iter()
            .any(|(_, rect)| rect.contains(mouse_pos))
    }

    pub fn render(&self, g: &mut Graphics) {
        self.panel.fill_background(g);
        self.highlight_selected_answer(g);
        self.writer.render(g);
        self.panel.render_border(g);
    }

    fn highlight_selected_answer(&self, g: &mut Graphics) {
        let (_, rect) = self
            .answers
            .iter()
            .find(|(a, _)| *a == self.selected_answer)
            .expect("Unknown answer");
        g.fill_rect(*rect, HIGHLIGHTED_BACKGROUND_COLOR);
        g.stroke_rect(*rect, DEFAULT_COLOR, ANSWER_SELECTION_LINE_THICKNESS);
    }

    pub fn answers_count(&self) -> usize {
        self.answers.len()
    }

    pub fn next_answer(&mut self) {
        let index = self
            .answers
            .iter()
            .position(|(a, _)| *a == self.selected_answer)
            .expect("Unknown current answer");
        let next_index = (index + 1) % self.answers.len();

        self.selected_answer = self.answers[next_index].0.clone()
    }

    pub fn previous_answer(&mut self) {
        let index = self
            .answers
            .iter()
            .position(|(a, _)| *a == self.selected_answer)
            .expect("Unknown current answer");
        let next_index = (index + self.answers.len() - 1) % self.answers.len();

        self.selected_answer = self.answers[next_index].0.clone()
    }

    pub fn change_answer_from_dir(&mut self, dir: Direction) {
        let vec = dir.delta_vec();
        if vec.y == 1 || (vec.y == 1 && vec.x == 1) {
            self.next_answer();
        } else {
            self.previous_answer();
        }
    }

    pub fn selected_answer(&self) -> A {
        self.selected_answer.clone()
    }
}

impl PopupPrompt<YesNoAnswer> {
    pub fn yes_no(question: ColoredText) -> Self {
        Self::new(question, YesNoAnswer::ALL)
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum YesNoAnswer {
    Yes,
    No,
}

impl DisplayColored for YesNoAnswer {
    fn display_colored(&self) -> ColoredText {
        match self {
            YesNoAnswer::Yes => "Yes".into(),
            YesNoAnswer::No => "No".into(),
        }
    }
}

impl YesNoAnswer {
    const ALL: [Self; 2] = [YesNoAnswer::Yes, YesNoAnswer::No];
    pub fn is_yes(&self) -> bool {
        *self == YesNoAnswer::Yes
    }

    pub fn is_no(&self) -> bool {
        *self == YesNoAnswer::No
    }
}
