use std::time::Duration;

use wasm_bindgen::{
    prelude::{wasm_bindgen, Closure},
    JsCast, JsValue,
};
use web_sys::{HtmlAudioElement, HtmlCanvasElement, HtmlImageElement};

use crate::{
    constants::{CANVAS_SIZE, DEBUG_INPUT},
    input::InputEvent,
    utils::{geometry::ScreenPoint, time::Time},
};

#[wasm_bindgen]
extern "C" {
    // Use `js_namespace` here to bind `console.log(..)` instead of just
    // `log(..)`
    #[wasm_bindgen(js_namespace = console)]
    pub fn log(s: &str);

    #[wasm_bindgen(js_namespace = console)]
    pub fn warn(s: &str);

    #[wasm_bindgen(js_namespace = console)]
    pub fn error(s: &str);

    #[wasm_bindgen(js_namespace = Date)]
    fn now() -> u32;

}
// used by stopwatch macro
pub fn js_now() -> Time {
    Time::from(Duration::from_millis(now().into()))
}

type TickType = dyn FnMut(f64, u32, u32, u32, u32, bool, Vec<JsValue>);

#[wasm_bindgen(module = "/src/js_bindings/rust-export.js")]
extern "C" {
    pub(crate) fn setTickClosure(closure: &Closure<TickType>);
}

#[macro_export]
macro_rules! console_log {
    // Note that this is using the `log` function imported above
    ($($t:tt)*) => ({use crate::js_bindings::log;log(&format_args!($($t)*).to_string())})
}
#[macro_export]
macro_rules! console_warn {
    // Note that this is using the `error` function imported above
    ($($t:tt)*) => ({use crate::js_bindings::warn;warn(&format_args!($($t)*).to_string())})
}
#[macro_export]
macro_rules! console_error {
    // Note that this is using the `error` function imported above
    ($($t:tt)*) => ({use crate::js_bindings::error;error(&format_args!($($t)*).to_string())})
}

// Array
#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(typescript_type = "Array<any>")]
    #[derive(Clone, Debug)]
    pub type Array;

    /// Creates a new empty array.
    #[wasm_bindgen(constructor)]
    pub fn new() -> Array;

    #[wasm_bindgen(static_method_of = Array, js_name = of)]
    pub fn of_two_f64(a: f64, b: f64) -> Array;
}

#[derive(Debug, Clone)]
pub struct Env {
    pub random: u64,
    pub delta: Duration,
    pub time: Time,
    pub mouse_down: bool,
    pub mouse_pos: ScreenPoint,
}

pub(crate) struct TickJsParams {
    pub random: f64,
    pub delta_ms: u32,
    pub elapsed_ms: u32,
    pub mouse_x: u32,
    pub mouse_y: u32,
    pub mouse_down: bool,
    pub events: Vec<JsValue>,
}

pub(crate) fn process_tick_params(params: TickJsParams) -> (Env, Vec<InputEvent>) {
    let random = params.random.to_bits();

    let delta = Duration::from_millis(params.delta_ms.into());
    let time = Time::from(Duration::from_millis(params.elapsed_ms.into()));

    let canvas = get_canvas();
    let events = params
        .events
        .iter()
        .filter_map(|raw_event| {
            if let Ok(event) = InputEvent::try_from(raw_event) {
                if DEBUG_INPUT {
                    console_log!("Received event: {event:?}");
                }
                Some(event)
            } else {
                if DEBUG_INPUT {
                    console_log!("Couldn't interpret event value: {raw_event:?}");
                }
                None
            }
        })
        .collect();

    let env = Env {
        random,
        delta,
        time,
        mouse_down: params.mouse_down,
        mouse_pos: updated_mouse_pos(&canvas, params.mouse_x, params.mouse_y),
    };

    (env, events)
}

fn calculate_canvas_scale(canvas: &HtmlCanvasElement) -> f64 {
    canvas.client_height() as f64 / CANVAS_SIZE.height
}

fn updated_mouse_pos(canvas: &HtmlCanvasElement, x: u32, y: u32) -> ScreenPoint {
    let scale = calculate_canvas_scale(canvas);

    // the canvas width can be too large: add an offset correction for x
    let diff_width = canvas.client_width() as f64 - (CANVAS_SIZE.width * scale);
    let new_x = (x as f64 - (diff_width / 2.0)) / scale;
    let new_y = y as f64 / scale;

    ScreenPoint::new(new_x, new_y)
}

const CANVAS_ID: &str = "canvas";
pub(crate) fn get_rendering_context() -> web_sys::CanvasRenderingContext2d {
    let context = get_canvas()
        .get_context("2d")
        .unwrap()
        .unwrap()
        .dyn_into::<web_sys::CanvasRenderingContext2d>()
        .unwrap();

    context
}

pub fn get_canvas() -> web_sys::HtmlCanvasElement {
    let window = web_sys::window().expect("no global `window` exists");
    let document = window.document().expect("should have a document on window");
    let canvas = document
        .get_element_by_id(CANVAS_ID)
        .expect("no canvas found");
    let canvas: web_sys::HtmlCanvasElement =
        canvas.dyn_into::<web_sys::HtmlCanvasElement>().unwrap();

    canvas
}

pub fn preload_audio(path: &str) {
    let audio = HtmlAudioElement::new_with_src(path).unwrap();
    audio.set_preload("true");
    web_sys::window()
        .unwrap()
        .document()
        .unwrap()
        .body()
        .unwrap()
        .append_child(&audio)
        .unwrap();
}

pub fn preload_image(path: &str) {
    let image = HtmlImageElement::new().unwrap();
    image.set_hidden(true);
    image.set_src(path);
    web_sys::window()
        .unwrap()
        .document()
        .unwrap()
        .body()
        .unwrap()
        .append_child(&image)
        .unwrap();
}

pub fn play_audio(path: &str, volume: f64) {
    let audio = HtmlAudioElement::new_with_src(path).unwrap();
    audio.set_volume(volume);
    // drop the future
    let _ = audio.play().expect("Failed to play the audio");
}
