use std::panic;

use crate::{
    console_error,
    input::InputEvent,
    js_bindings::{setTickClosure, Env},
    ui::graphics::Graphics,
    utils::geometry::ScreenRect,
};

use crate::constants::{BACKGROUND_COLOR, CANVAS_SIZE};
use crate::js_bindings::{get_canvas, get_rendering_context, process_tick_params, TickJsParams};
use crate::screen::app_state::AppState;

use wasm_bindgen::prelude::*;

fn resize_canvas() {
    let canvas = get_canvas();
    canvas.set_width(CANVAS_SIZE.width as u32);
    canvas.set_height(CANVAS_SIZE.height as u32);
}

#[wasm_bindgen]
pub async fn init_game() {
    panic::set_hook(Box::new(console_error_panic_hook::hook));
    resize_canvas();

    let mut state = AppState::init_app();

    let (channel_send, channel_receive) = async_channel::unbounded::<(Env, Vec<InputEvent>)>();

    let closure = Closure::new(
        move |random: f64,
              delta_ms: u32,
              elapsed_ms: u32,
              mouse_x: u32,
              mouse_y: u32,
              mouse_down: bool,
              events: Vec<JsValue>| {
            let msg = process_tick_params(TickJsParams {
                random,
                delta_ms,
                elapsed_ms,
                mouse_x,
                mouse_y,
                mouse_down,
                events,
            });

            let res = channel_send.send_blocking(msg);
            if res.is_err() {
                console_error!("Failed to send message");
            }
        },
    );

    setTickClosure(&closure);

    let context = get_rendering_context();
    let mut g = Graphics::new(context);
    while let Ok((env, events)) = channel_receive.recv().await {
        g.fill_rect(ScreenRect::from_size(CANVAS_SIZE), BACKGROUND_COLOR);

        for event in events {
            state = state.handle_event(&env, event);
        }
        state = state.tick(&env, &mut g);
    }

    console_error!("The message sender got dropped. Exiting...");
}
