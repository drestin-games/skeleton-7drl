use std::fmt::Debug;

use crate::{
    gameplay::{self, screen::GameplayScreenState},
    input::InputEvent,
    js_bindings::Env,
    ui::graphics::Graphics,
};

use super::splash::SplashScreenState;

pub enum AppState {
    SplashScreen(SplashScreenState),
    GameplayScreen(gameplay::screen::GameplayScreenState),
}

impl Debug for AppState {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::GameplayScreen(_) => f.write_str("GameplayScreen{..}"),
            _ => self.fmt(f),
        }
    }
}

impl AppState {
    pub const fn init_app() -> Self {
        Self::SplashScreen(SplashScreenState)
    }
}

impl AppState {
    pub fn handle_event(self, env: &Env, event: InputEvent) -> AppState {
        match self {
            AppState::SplashScreen(s) => s.handle_event(env, event),
            AppState::GameplayScreen(s) => s.handle_event(env, event),
        }
    }

    pub fn tick(self, env: &Env, g: &mut Graphics) -> AppState {
        match self {
            AppState::SplashScreen(s) => {
                s.render(g);
                s.into()
            }
            AppState::GameplayScreen(s) => s.tick(env, g),
        }
    }
}

impl From<SplashScreenState> for AppState {
    fn from(value: SplashScreenState) -> Self {
        Self::SplashScreen(value)
    }
}

impl From<GameplayScreenState> for AppState {
    fn from(value: GameplayScreenState) -> Self {
        Self::GameplayScreen(value)
    }
}
