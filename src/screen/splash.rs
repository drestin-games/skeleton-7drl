use crate::{
    constants::{CANVAS_RECT, TEXT_FONT},
    gameplay::screen::GameplayScreenState,
    input::InputEvent,
    js_bindings::Env,
    ui::{color::Color, colored_text::Colorable, graphics::Graphics},
    utils::geometry::{Align, ScreenRect, ScreenSize, ScreenVec},
};

use super::app_state::AppState;

#[derive(Debug)]
pub struct SplashScreenState;

impl SplashScreenState {
    pub fn render(&self, g: &mut Graphics) {
        g.set_font(&TEXT_FONT);
        let text = "hell" + "o\nwor".colored(Color::BLUE) + "lp";
        let bounding = g.write_text_in_rect(&text, CANVAS_RECT, Align::TOP_RIGHT);
        g.stroke_rect(bounding, Color::BLUE, 2.0);
        g.fill_rect(
            ScreenRect::new(
                CANVAS_RECT.center() - ScreenVec::splat(2.0),
                ScreenSize::splat(4.0),
            ),
            Color::RED,
        );
    }

    pub fn handle_event(self, env: &Env, event: InputEvent) -> AppState {
        match event {
            InputEvent::Click | InputEvent::Enter => {
                GameplayScreenState::with_seed(env.random).into()
            }
            _ => self.into(),
        }
    }
}
