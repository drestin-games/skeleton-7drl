use wasm_bindgen::JsValue;

use crate::utils::geometry::Direction;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum InputEvent {
    Resize,
    Enter,
    Click,
    Escape,
    Space,
    Arrow(Direction),
    Wait,
    DescendStairs,
    MouseMove,
}

impl TryFrom<&JsValue> for InputEvent {
    type Error = String;

    fn try_from(value: &JsValue) -> Result<Self, Self::Error> {
        if let Some(str) = value.as_string() {
            use Direction::*;
            use InputEvent::*;
            match str.as_str() {
                "Numpad7" | "KeyQ" => Ok(Arrow(NorthWest)),
                "Numpad9" | "KeyE" => Ok(Arrow(NorthEast)),
                "Numpad1" | "KeyZ" => Ok(Arrow(SouthWest)),
                "Numpad3" | "KeyC" => Ok(Arrow(SouthEast)),
                "ArrowLeft" | "Numpad4" | "KeyA" => Ok(Arrow(West)),
                "ArrowRight" | "Numpad6" | "KeyD" => Ok(Arrow(East)),
                "ArrowUp" | "Numpad8" | "KeyW" => Ok(Arrow(North)),
                "ArrowDown" | "Numpad2" | "KeyX" => Ok(Arrow(South)),
                "Numpad5" | "KeyS" => Ok(Wait),
                "Enter" => Ok(Enter),
                "Escape" => Ok(Escape),
                "Space" => Ok(Space),
                "MouseDown" => Ok(Click),
                "MouseMove" => Ok(MouseMove),
                "Resize" => Ok(Resize),
                "Period" => Ok(DescendStairs),
                _ => Err(format!("Unkown event string: {str}")),
            }
        } else {
            Err(format!("Event is not a string: {value:?}"))
        }
    }
}
