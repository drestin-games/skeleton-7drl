#![feature(associated_type_defaults)]
#![feature(const_fn_floating_point_arithmetic)]
#![feature(is_some_and)]
#![feature(option_result_contains)]
#![feature(test)]
#![deny(unused_must_use)]

pub mod constants;
mod gameplay;
mod input;
pub mod js_bindings;
mod main_loop;
mod screen;
pub mod ui;
pub mod utils;
