use std::ops::RangeBounds;

use super::geometry::GridPos;

/// Wrapper around the actual rng library to enforce exclusive reference on use
/// and implement Serialize/Deserialize.
#[derive(Debug)]
pub struct MyRng(fastrand::Rng);

impl MyRng {
    pub fn from_seed(seed: u64) -> Self {
        Self(fastrand::Rng::with_seed(seed))
    }

    pub fn choose<'a, T, S: AsRef<[T]>>(&mut self, slice_like: &'a S) -> Option<&'a T> {
        let slice = slice_like.as_ref();
        match slice.len() {
            0 => None,
            1 => Some(&slice[0]),
            length => {
                let i = self.usize(0..length);
                Some(&slice[i])
            }
        }
    }

    pub fn choose_iter<T>(&mut self, iter: impl Iterator<Item = T>) -> Option<T> {
        let mut vec = iter.collect::<Vec<T>>();
        match vec.len() {
            0 => None,
            1 => Some(vec.remove(0)),
            length => {
                let i = self.usize(0..length);
                Some(vec.swap_remove(i))
            }
        }
    }

    pub fn choose_iter_weighted<T>(&mut self, iter: impl Iterator<Item = (T, f64)>) -> Option<T> {
        let mut vec = iter.collect::<Vec<_>>();
        match vec.len() {
            0 => None,
            1 => Some(vec.remove(0).0),
            length => {
                let weights_sum = vec.iter().map(|(_, w)| w).sum();
                let chosen_random = self.f64(0.0, weights_sum);

                let mut i = 0;
                let mut sum = 0.0;

                while i < length {
                    sum += vec[i].1;
                    if sum > chosen_random {
                        return Some(vec.swap_remove(i).0);
                    }
                    i += 1;
                }
                unreachable!();
            }
        }
    }

    #[inline]
    pub fn _shuffle<T, S: AsMut<[T]>>(&mut self, slice_like: &mut S) {
        self.0.shuffle(slice_like.as_mut())
    }

    #[inline]
    pub fn i32(&mut self, range: impl RangeBounds<i32>) -> i32 {
        self.0.i32(range)
    }

    #[inline]
    pub fn usize(&mut self, range: impl RangeBounds<usize>) -> usize {
        self.0.usize(range)
    }

    pub fn f64(&mut self, min: f64, max: f64) -> f64 {
        self.0.f64() * max + min
    }

    pub fn pos(&mut self, min: GridPos, max: GridPos) -> GridPos {
        let x = self.i32(min.x..max.x);
        let y = self.i32(min.y..max.y);
        GridPos::new(x, y)
    }
}

// impl Serialize for MyRng {
//     fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
//     where
//         S: serde::Serializer,
//     {
//         self.0.get_seed().serialize(serializer)
//     }
// }

// impl<'de> Deserialize<'de> for MyRng {
//     fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
//     where
//         D: serde::Deserializer<'de>,
//     {
//         let seed = u64::deserialize(deserializer)?;

//         Ok(Self::from_seed(seed))
//     }
// }

pub trait Choosable<T> {
    fn choose(self, rng: &mut MyRng) -> Option<T>;
}

pub trait ChoosableWeighted<T> {
    fn choose_weighted(self, rng: &mut MyRng) -> Option<T>;
}
pub trait ChoosableMin<T>: Choosable<T> {
    fn choose_min(self, rng: &mut MyRng) -> Option<T>
    where
        T: Ord + Eq;
    fn choose_min_by_key<F, U>(self, rng: &mut MyRng, f: F) -> Option<T>
    where
        U: Ord + Eq,
        F: FnMut(&T) -> U;
}

impl<T, I> Choosable<T> for I
where
    I: Iterator<Item = T>,
{
    fn choose(self, rng: &mut MyRng) -> Option<T> {
        rng.choose_iter(self)
    }
}

impl<T, I> ChoosableMin<T> for I
where
    I: Iterator<Item = T> + Clone,
{
    fn choose_min(self, rng: &mut MyRng) -> Option<T>
    where
        T: Ord + Eq,
    {
        let clone = self.clone();
        self.min()
            .and_then(move |min| clone.filter(|t| *t == min).choose(rng))
    }

    fn choose_min_by_key<F, U>(self, rng: &mut MyRng, mut f: F) -> Option<T>
    where
        U: Ord + Eq,
        F: FnMut(&T) -> U,
    {
        let clone = self.clone();
        self.map(|t| f(&t))
            .min()
            .and_then(move |min| clone.filter(|t| f(t) == min).choose(rng))
    }
}

impl<T, I> ChoosableWeighted<T> for I
where
    I: Iterator<Item = (T, f64)>,
{
    fn choose_weighted(self, rng: &mut MyRng) -> Option<T> {
        rng.choose_iter_weighted(self)
    }
}
