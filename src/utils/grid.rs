use std::ops::{Index, IndexMut};

use super::geometry::{GridPos, GridRect, GridSize, GridVec};

#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Grid<T> {
    rect: GridRect,
    data: Vec<T>,
}

impl<T> Default for Grid<T> {
    fn default() -> Self {
        Self {
            rect: Default::default(),
            data: Default::default(),
        }
    }
}

impl<T: Clone> Grid<T> {
    pub fn filled(width: usize, height: usize, elem: T) -> Self {
        let rect = GridRect::new(
            GridPos::origin(),
            GridSize::new(width as i32, height as i32),
        );

        Self::filled_rect(rect, elem)
    }

    pub fn filled_rect(rect: GridRect, elem: T) -> Self {
        Self::from_fn_rect(rect, |_pos| elem.clone())
    }
}

impl<T> Grid<T> {
    pub fn _from_fn(width: usize, height: usize, f: impl FnMut(GridPos) -> T) -> Self {
        let rect = GridRect::new(
            GridPos::origin(),
            GridSize::new(width as i32, height as i32),
        );

        Self::from_fn_rect(rect, f)
    }

    pub fn from_fn_rect(rect: GridRect, mut f: impl FnMut(GridPos) -> T) -> Self {
        let mut data = Vec::with_capacity(rect.width() as usize * rect.height() as usize);
        for y in rect.y_range() {
            for x in rect.x_range() {
                data.push(f(GridPos::new(x, y)));
            }
        }

        Self { rect, data }
    }

    pub fn iter(&self) -> impl Iterator<Item = (GridPos, &T)> + Clone + '_ {
        self.data
            .iter()
            .enumerate()
            .map(|(i, elem)| (self.pos_from_index(i), elem))
    }

    pub fn iter_mut(&mut self) -> impl Iterator<Item = (GridPos, &mut T)> + '_ {
        let rect = self.rect;
        self.data
            .iter_mut()
            .enumerate()
            .map(move |(i, elem)| (pos_from_index(i, rect), elem))
    }

    #[inline]
    pub fn get(&self, pos: GridPos) -> Option<&T> {
        self.index_from_pos(pos).map(|i| &self.data[i])
    }

    pub fn set(&mut self, pos: GridPos, value: T) {
        if let Some(cell) = self.get_mut(pos) {
            *cell = value;
        }
    }

    #[inline]
    pub fn get_mut(&mut self, pos: GridPos) -> Option<&mut T> {
        self.index_from_pos(pos).map(|i| &mut self.data[i])
    }

    #[inline]
    pub fn width(&self) -> i32 {
        self.rect.width()
    }

    #[inline]
    pub fn height(&self) -> i32 {
        self.rect.height()
    }

    #[inline]
    pub fn _contains(&self, pos: GridPos) -> bool {
        self.rect.contains(pos)
    }

    #[inline]
    fn index_from_pos(&self, pos: GridPos) -> Option<usize> {
        if self.rect.contains(pos) {
            let pos = pos - self.rect.min();
            Some((pos.x + pos.y * self.width()) as usize)
        } else {
            None
        }
    }

    #[inline]
    fn index_from_pos_unchecked(&self, pos: GridPos) -> usize {
        (pos.x - self.rect.min_x() + (pos.y - self.rect.min_y()) * self.width()) as usize
    }

    #[inline]
    fn pos_from_index(&self, i: usize) -> GridPos {
        pos_from_index(i, self.rect)
    }

    pub fn rect(&self) -> GridRect {
        self.rect
    }

    pub fn translate(&mut self, offset: GridVec) -> &mut Self {
        self.rect = self.rect.translate(offset);
        self
    }

    pub fn set_origin(&mut self, origin: GridPos) -> &mut Self {
        self.rect.origin = origin;
        self
    }
}

impl<T> Index<GridPos> for Grid<T> {
    type Output = T;

    fn index(&self, pos: GridPos) -> &Self::Output {
        &self.data[self.index_from_pos_unchecked(pos)]
    }
}

impl<T> IndexMut<GridPos> for Grid<T> {
    fn index_mut(&mut self, pos: GridPos) -> &mut Self::Output {
        let i = self.index_from_pos_unchecked(pos);
        &mut self.data[i]
    }
}
#[inline]
fn pos_from_index(i: usize, rect: GridRect) -> GridPos {
    GridPos::new(x_from_index(i, &rect), y_from_index(i, &rect))
}

#[inline]
fn x_from_index(i: usize, rect: &GridRect) -> i32 {
    (i as i32 % rect.width()) + rect.min_x()
}

#[inline]
fn y_from_index(i: usize, rect: &GridRect) -> i32 {
    (i as i32 / rect.width()) + rect.min_y()
}
