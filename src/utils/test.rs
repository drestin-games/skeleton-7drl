#[cfg(test)]
mod tests {
    extern crate test;
    use test::{black_box, Bencher};

    use crate::utils::{
        geometry::{GridPos, GridRect, GridSize},
        shapes::rectangle,
    };

    #[bench]
    fn test_iter(b: &mut Bencher) {
        b.iter(|| {
            let n = black_box(20000);
            rectangle(GridRect::new(
                GridPos::new(-n / 2, -n / 3),
                GridSize::new(n, n / 2),
            ))
            .count()
        })
    }

    #[bench]
    fn test_collect(b: &mut Bencher) {
        b.iter(|| {
            let n = black_box(20000);
            rectangle(GridRect::new(
                GridPos::new(-n / 2, -n / 3),
                GridSize::new(n, n / 2),
            ))
            .into_iter()
            .count()
        })
        // b.iter(|| -> i32 {
        //     let n = black_box(2000);
        //     rectangle_border_vec(
        //         GridRect::new(GridPos::new(-n / 2, -n / 3), GridSize::new(n, n / 2)),
        //         n / 8,
        //     )
        //     .into_iter()
        //     .fold(0, |n, p| n + p.x - p.y)
        // })
    }
}
