use std::{
    ops::{Add, Sub},
    time::Duration,
};

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct Time(Duration);

impl Time {
    pub fn as_secs_f64(&self) -> f64 {
        self.0.as_secs_f64()
    }
}

impl From<Duration> for Time {
    fn from(duration: Duration) -> Self {
        Time(duration)
    }
}

impl Add<Duration> for Time {
    type Output = Time;

    fn add(self, rhs: Duration) -> Self::Output {
        Time(self.0 + rhs)
    }
}

impl Sub<Duration> for Time {
    type Output = Time;

    fn sub(self, rhs: Duration) -> Self::Output {
        Time(self.0 + rhs)
    }
}

impl Sub<Time> for Time {
    type Output = Duration;

    /// PANICS if rhs is after self
    fn sub(self, rhs: Time) -> Self::Output {
        self.0 - rhs.0
    }
}

#[macro_export]
macro_rules! stopwatch {
    {$name:literal: $($s:stmt)+} => {
        let __start_time = {use crate::js_bindings::js_now; js_now()};

        $($s)+

        {use crate::utils::rust::DebugFunction; ({use crate::js_bindings::js_now; js_now()}-__start_time).debug($name)};
    };
}
