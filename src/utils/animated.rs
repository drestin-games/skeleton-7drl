use std::{
    ops::{Add, Mul, Sub},
    time::Duration,
};

pub trait Animated
where
    Self: Sized,
{
    type Item;
    fn get(&self, elapsed: Duration) -> Self::Item;
    fn duration(&self) -> Duration;

    fn chain<U>(self, other: U) -> Chain<Self, U>
    where
        U: Animated<Item = Self::Item>,
    {
        Chain { a: self, b: other }
    }

    fn rev(self) -> Rev<Self> {
        Rev { animated: self }
    }

    fn chain_rev(self) -> Chain<Self, Rev<Self>>
    where
        Self: Clone,
    {
        let clone = self.clone();
        self.chain(clone.rev())
    }

    fn repeated(self, times: u32) -> Repeat<Self> {
        Repeat { elem: self, times }
    }

    fn repeated_infinitly(self) -> InfiniteRepeat<Self> {
        InfiniteRepeat { elem: self }
    }
}

#[derive(Debug, Clone)]
pub struct Interpolation<T> {
    from: T,
    to: T,
    duration: Duration,
    easing: Easing,
}

impl<T> Interpolation<T> {
    pub fn new(from: T, to: T, duration: Duration, easing: Easing) -> Self {
        Self {
            from,
            to,
            duration,
            easing,
        }
    }
}

impl Interpolation<f64> {
    // Interpolation between 0.0 and 1.0 included
    pub fn zero_one(duration: Duration, easing: Easing) -> Self {
        Interpolation::new(0.0, 1.0, duration, easing)
    }
}

impl<T, V> Animated for Interpolation<T>
where
    T: Clone + Add<V, Output = T> + Sub<T, Output = V>,
    V: Mul<f64, Output = V>,
{
    type Item = T;

    fn get(&self, elapsed: Duration) -> Self::Item {
        let ratio = elapsed.as_secs_f64() / self.duration().as_secs_f64();
        let ratio = self.easing.interpolate(ratio);
        self.from.clone() + ((self.to.clone() - self.from.clone()) * ratio)
    }

    fn duration(&self) -> Duration {
        self.duration
    }
}

pub struct Chain<A, B> {
    a: A,
    b: B,
}

impl<A, B> Animated for Chain<A, B>
where
    A: Animated,
    B: Animated<Item = A::Item>,
{
    type Item = A::Item;

    fn get(&self, elapsed: Duration) -> Self::Item {
        let a_duration = self.a.duration();
        if elapsed <= a_duration {
            self.a.get(elapsed)
        } else {
            self.b.get(elapsed - a_duration)
        }
    }

    fn duration(&self) -> Duration {
        self.a.duration().saturating_add(self.b.duration())
    }
}

pub struct Rev<A> {
    animated: A,
}

impl<A> Animated for Rev<A>
where
    A: Animated,
{
    type Item = A::Item;

    fn get(&self, elapsed: Duration) -> Self::Item {
        self.animated.get(self.duration() - elapsed)
    }

    fn duration(&self) -> Duration {
        self.animated.duration()
    }
}
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Easing {
    Linear,
    // InCubic,
    // OutCubic,
    // InOutCubic,
}

// https://easings.net/
impl Easing {
    pub fn interpolate(&self, t: f64) -> f64 {
        use Easing::*;
        let t = t.clamp(0.0, 1.0);
        match self {
            Linear => t,
            // InCubic => 1.0 - (1.0 - t).powi(3),
            // OutCubic => t * t * t,
            // InOutCubic => {
            //     if t < 0.5 {
            //         4.0 * t * t * t
            //     } else {
            //         1.0 - (-2.0 * t + 2.0).powi(3) / 2.0
            //     }
            // }
        }
    }
}

pub struct Constant<T> {
    value: T,
    duration: Duration,
}

impl<T> Constant<T> {
    pub fn new(value: T, duration: Duration) -> Self {
        Self { value, duration }
    }
}

impl<T: Clone> Animated for Constant<T> {
    type Item = T;

    fn get(&self, _elapsed: Duration) -> Self::Item {
        self.value.clone()
    }

    fn duration(&self) -> Duration {
        self.duration
    }
}

pub struct Repeat<A> {
    elem: A,
    times: u32,
}

impl<A> Animated for Repeat<A>
where
    A: Animated,
{
    type Item = A::Item;

    fn get(&self, elapsed: Duration) -> Self::Item {
        let elem_duration = self.elem.duration();
        let elapsed = if elapsed >= self.duration() {
            elem_duration
        } else {
            Duration::from_millis(elapsed.as_millis() as u64 % elem_duration.as_millis() as u64)
        };
        self.elem.get(elapsed)
    }

    fn duration(&self) -> Duration {
        self.elem.duration().saturating_mul(self.times)
    }
}

pub struct InfiniteRepeat<A> {
    elem: A,
}

impl<A> Animated for InfiniteRepeat<A>
where
    A: Animated,
{
    type Item = A::Item;

    fn get(&self, elapsed: Duration) -> Self::Item {
        let elem_duration = self.elem.duration();
        let elapsed =
            Duration::from_millis(elapsed.as_millis() as u64 % elem_duration.as_millis() as u64);

        self.elem.get(elapsed)
    }

    fn duration(&self) -> Duration {
        Duration::MAX
    }
}
