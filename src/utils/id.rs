use std::fmt::Debug;

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Id<const T: u8>(u32);

impl<const T: u8> Debug for Id<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

#[derive(Default, Debug)]
pub struct IdGenerator {
    next_id: u32,
}

impl IdGenerator {
    pub fn generate<const T: u8>(&mut self) -> Id<T> {
        let id = Id(self.next_id);
        self.next_id += 1;

        id
    }
}

pub type ActorId = Id<0>;
