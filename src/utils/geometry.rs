use std::{
    cmp::Ordering,
    ops::{Add, AddAssign, Div, Mul, Sub},
};

use euclid::{
    num::{One, Zero},
    Box2D, Point2D, Rect, SideOffsets2D, Size2D, Transform2D, Vector2D,
};

pub struct ScreenSpace;
pub type ScreenSpaceType = f64;
pub type ScreenPoint = Point2D<ScreenSpaceType, ScreenSpace>;
pub type ScreenSize = Size2D<ScreenSpaceType, ScreenSpace>;
pub type ScreenSideOffsets = SideOffsets2D<ScreenSpaceType, ScreenSpace>;
pub type ScreenVec = Vector2D<ScreenSpaceType, ScreenSpace>;
pub type ScreenBox = Box2D<ScreenSpaceType, ScreenSpace>;
pub type ScreenRect = Rect<ScreenSpaceType, ScreenSpace>;

pub type PanelSpace = ScreenSpace;
pub type PanelSpaceType = i32;
pub type PanelPos = Point2D<PanelSpaceType, PanelSpace>;
pub type PanelSize = Size2D<PanelSpaceType, PanelSpace>;

pub struct WorldSpace;
pub type WorldSpaceType = f64;
pub type WorldPoint = Point2D<WorldSpaceType, WorldSpace>;
pub type WorldVec = Vector2D<WorldSpaceType, WorldSpace>;
pub type WorldSize = Size2D<WorldSpaceType, WorldSpace>;
pub type WorldRect = Rect<WorldSpaceType, WorldSpace>;

pub type CameraTransform = Transform2D<ScreenSpaceType, WorldSpace, ScreenSpace>;

pub type GridSpace = WorldSpace;
pub type GridSpaceType = i32;
pub type GridPos = Point2D<GridSpaceType, GridSpace>;
pub type GridVec = Vector2D<GridSpaceType, GridSpace>;
pub type GridSize = Size2D<GridSpaceType, GridSpace>;
pub type GridRect = Rect<GridSpaceType, GridSpace>;
// pub type GridBox = Box2D<GridSpaceType, GridSpace>;

pub struct SpriteSheetSpace;
pub type SpriteSheetSpaceType = f64;
pub type SpriteSheetRect = Rect<SpriteSheetSpaceType, SpriteSheetSpace>;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub enum Direction {
    North,
    NorthEast,
    East,
    SouthEast,
    South,
    SouthWest,
    West,
    NorthWest,
}
use Direction::*;

use crate::constants::{CANVAS_RECT, DIAGONAL_MOVE_COST, ORTHOGAL_MOVE_COST};
impl Direction {
    pub const ALL_DIR_4: [Direction; 4] = [North, East, South, West];

    pub const ALL_DIR_8: [Direction; 8] = [
        North, NorthEast, East, SouthEast, South, SouthWest, West, NorthWest,
    ];

    pub fn delta_vec(&self) -> GridVec {
        match self {
            North => GridVec::new(0, -1),
            NorthEast => GridVec::new(1, -1),
            East => GridVec::new(1, 0),
            SouthEast => GridVec::new(1, 1),
            South => GridVec::new(0, 1),
            SouthWest => GridVec::new(-1, 1),
            West => GridVec::new(-1, 0),
            NorthWest => GridVec::new(-1, -1),
        }
    }

    pub fn is_diagonal(&self) -> bool {
        match self {
            North | East | South | West => false,
            NorthEast | SouthEast | SouthWest | NorthWest => true,
        }
    }

    pub fn is_orthogonal(&self) -> bool {
        !self.is_diagonal()
    }

    pub fn rev(&self) -> Self {
        match self {
            North => South,
            NorthEast => SouthWest,
            East => West,
            SouthEast => NorthWest,
            South => North,
            SouthWest => NorthEast,
            West => East,
            NorthWest => SouthEast,
        }
    }

    pub fn cost(&self) -> i32 {
        if self.is_orthogonal() {
            ORTHOGAL_MOVE_COST
        } else {
            DIAGONAL_MOVE_COST
        }
    }
}

impl From<Direction> for GridVec {
    fn from(value: Direction) -> Self {
        value.delta_vec()
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum GridConnection {
    Four,
    Eight,
}

impl GridConnection {
    pub fn are_neighbours(&self, a: GridPos, b: GridPos) -> bool {
        use GridConnection::*;
        if a == b {
            false
        } else {
            let delta = (b - a).abs();
            match self {
                Four => delta.x + delta.y == 1,
                Eight => delta.x <= 1 && delta.y <= 1,
            }
        }
    }

    pub fn directions(&self) -> &'static [Direction] {
        use GridConnection::*;
        match self {
            Four => Direction::ALL_DIR_4.as_slice(),
            Eight => Direction::ALL_DIR_8.as_slice(),
        }
    }

    pub fn neighbours(&self, pos: GridPos) -> impl Iterator<Item = GridPos> + '_ {
        self.directions()
            .iter()
            .map(move |dir| pos + dir.delta_vec())
    }

    pub fn neighbours_with_dir(
        &self,
        pos: GridPos,
    ) -> impl Iterator<Item = (GridPos, Direction)> + '_ {
        self.directions()
            .iter()
            .map(move |dir| (pos + dir.delta_vec(), *dir))
    }

    pub fn neighbours_with_cost(&self, pos: GridPos) -> impl Iterator<Item = (GridPos, i32)> + '_ {
        self.neighbours_with_dir(pos)
            .map(|(p, dir)| (p, dir.cost()))
    }

    pub fn is_valid_dir(&self, dir: Direction) -> bool {
        use GridConnection::*;
        match self {
            Four => dir.is_orthogonal(),
            Eight => true,
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub struct Edge {
    pub tile: GridPos,
    pub side: EdgeSide,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum EdgeSide {
    North,
    West,
}

impl EdgeSide {
    pub fn _direction(&self) -> Direction {
        match self {
            EdgeSide::North => Direction::North,
            EdgeSide::West => Direction::West,
        }
    }
}

impl Edge {
    pub fn _new(pos: GridPos, edge_type: EdgeSide) -> Self {
        Self {
            tile: pos,
            side: edge_type,
        }
    }

    pub fn _between(a: GridPos, b: GridPos) -> Option<Self> {
        a.try_dir(b).and_then(|dir| {
            if dir.is_orthogonal() {
                let edge = match dir {
                    North => Self::_new(a, EdgeSide::North),
                    South => Self::_new(b, EdgeSide::North),
                    West => Self::_new(a, EdgeSide::West),
                    East => Self::_new(b, EdgeSide::West),
                    _ => unreachable!(),
                };
                Some(edge)
            } else {
                None
            }
        })
    }

    pub fn _other_tile(&self) -> GridPos {
        self.tile + self.side._direction().delta_vec()
    }

    /// The corner are oriented clockwise around self.tile.
    pub fn corners(&self) -> (Corner, Corner) {
        let (a, b) = match self.side {
            EdgeSide::North => (self.tile, self.tile + GridVec::new(1, 0)),
            EdgeSide::West => (self.tile + GridVec::new(0, 1), self.tile),
        };

        (Corner(a), Corner(b))
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct OrientedEdge {
    edge: Edge,
    /// if true, edge.tile is in the inside
    tile_inside: bool,
}

impl OrientedEdge {
    pub fn between(inside: GridPos, outside: GridPos) -> Option<Self> {
        Edge::_between(inside, outside).map(|edge| Self {
            edge,
            tile_inside: edge.tile == inside,
        })
    }

    /// The corners are oriented clockwise around the inside
    pub fn corners(&self) -> (Corner, Corner) {
        let (a, b) = self.edge.corners();
        if self.tile_inside {
            (a, b)
        } else {
            (b, a)
        }
    }

    pub fn tile(&self) -> GridPos {
        self.edge.tile
    }
}

/// The north-west corner of `tile`.
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub struct Corner(pub GridPos);

impl Corner {
    pub fn _touching_tiles(&self) -> impl Iterator<Item = GridPos> + '_ {
        use Direction::*;
        [
            NorthWest.delta_vec(),
            North.delta_vec(),
            West.delta_vec(),
            GridVec::zero(),
        ]
        .into_iter()
        .map(|delta| self.0 + delta)
    }
}

impl Ord for Corner {
    fn cmp(&self, other: &Self) -> Ordering {
        self.0.y.cmp(&other.0.y).then(self.0.x.cmp(&other.0.x))
    }
}

impl PartialOrd for Corner {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

pub trait GridPosExtra {
    fn try_dir(self, other: Self) -> Option<Direction>;
}

impl GridPosExtra for GridPos {
    fn try_dir(self, other: Self) -> Option<Direction> {
        let delta = other - self;
        match (delta.x, delta.y) {
            (0, -1) => Some(North),
            (1, -1) => Some(NorthEast),
            (1, 0) => Some(East),
            (1, 1) => Some(SouthEast),
            (0, 1) => Some(South),
            (-1, 1) => Some(SouthWest),
            (-1, 0) => Some(West),
            (-1, -1) => Some(NorthWest),
            _ => None,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct OrderedPos(GridPos);

impl Ord for OrderedPos {
    fn cmp(&self, other: &Self) -> Ordering {
        self.0.y.cmp(&other.0.y).then(self.0.x.cmp(&other.0.x))
    }
}

impl PartialOrd for OrderedPos {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

pub fn _find_tooltip_rect(tooltip_size: ScreenSize, mouse_pos: ScreenPoint) -> ScreenRect {
    // bottom-right
    let rect_intent = ScreenRect::new(mouse_pos, tooltip_size);
    if CANVAS_RECT.contains_rect(&rect_intent) {
        return rect_intent;
    }

    // bottom-left
    let rect_intent = ScreenRect::new(
        mouse_pos - ScreenVec::new(tooltip_size.width, 0.0),
        tooltip_size,
    );
    if CANVAS_RECT.contains_rect(&rect_intent) {
        return rect_intent;
    }

    // top-right
    let rect_intent = ScreenRect::new(
        mouse_pos - ScreenVec::new(0.0, tooltip_size.height),
        tooltip_size,
    );
    if CANVAS_RECT.contains_rect(&rect_intent) {
        return rect_intent;
    }

    // top-left, returned anyway
    ScreenRect::new(mouse_pos - tooltip_size, tooltip_size)
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum HAlign {
    Left,
    Center,
    Right,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum VAlign {
    Top,
    Middle,
    Bottom,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Align {
    pub v_align: VAlign,
    pub h_align: HAlign,
}

use HAlign::*;
use VAlign::*;
impl Align {
    pub const TOP_LEFT: Self = Self::new(Top, Left);
    pub const TOP_CENTER: Self = Self::new(Top, Center);
    pub const TOP_RIGHT: Self = Self::new(Top, Right);
    pub const MIDDLE_LEFT: Self = Self::new(Middle, Left);
    pub const MIDDLE_CENTER: Self = Self::new(Middle, Center);
    pub const MIDDLE_RIGHT: Self = Self::new(Middle, Right);
    pub const BOTTOM_LEFT: Self = Self::new(Bottom, Left);
    pub const BOTTOM_CENTER: Self = Self::new(Bottom, Center);
    pub const BOTTOM_RIGHT: Self = Self::new(Bottom, Right);

    pub const fn new(v_align: VAlign, h_align: HAlign) -> Self {
        Self { v_align, h_align }
    }

    pub fn with_v_align(&self, v_align: VAlign) -> Self {
        Self { v_align, ..*self }
    }

    pub fn with_h_align(&self, h_align: HAlign) -> Self {
        Self { h_align, ..*self }
    }
}

pub trait RectExtra<T, U>
where
    Self: Sized,
{
    fn inner_rect_same(&self, padding: T) -> Self;
    fn outer_rect_same(&self, margin: T) -> Self;
    fn hsplit(&self, split_x: T) -> (Self, Self);
    fn vsplit(&self, split_y: T) -> (Self, Self);
    fn anchor(&self, align: Align) -> Point2D<T, U>;
    fn from_anchor(anchor: Point2D<T, U>, size: Size2D<T, U>, align: Align) -> Self;
    fn aligned_inside(&self, container: &Self, align: Align) -> Self;
}

impl<T, U> RectExtra<T, U> for Rect<T, U>
where
    T: Clone
        + Copy
        + Add<Output = T>
        + Sub<Output = T>
        + Mul<T, Output = T>
        + Div<T, Output = T>
        + PartialOrd
        + Zero
        + One,
{
    fn inner_rect_same(&self, padding: T) -> Self {
        self.inner_rect(SideOffsets2D::new_all_same(padding))
    }

    fn outer_rect_same(&self, margin: T) -> Self {
        self.outer_rect(SideOffsets2D::new_all_same(margin))
    }

    fn hsplit(&self, split_x: T) -> (Self, Self) {
        let zero = T::zero();
        (
            self.inner_rect(SideOffsets2D::new(zero, self.width() - split_x, zero, zero)),
            self.inner_rect(SideOffsets2D::new(zero, zero, zero, split_x)),
        )
    }

    fn vsplit(&self, split_y: T) -> (Self, Self) {
        let zero = T::zero();
        (
            self.inner_rect(SideOffsets2D::new(
                zero,
                zero,
                self.height() - split_y,
                zero,
            )),
            self.inner_rect(SideOffsets2D::new(split_y, zero, zero, zero)),
        )
    }

    fn anchor(&self, align: Align) -> Point2D<T, U> {
        let anchor_x = match align.h_align {
            HAlign::Left => self.min_x(),
            HAlign::Center => self.center().x,
            HAlign::Right => self.max_x(),
        };
        let anchor_y = match align.v_align {
            VAlign::Top => self.min_y(),
            VAlign::Middle => self.center().y,
            VAlign::Bottom => self.max_y(),
        };

        Point2D::new(anchor_x, anchor_y)
    }

    fn from_anchor(anchor: Point2D<T, U>, size: Size2D<T, U>, align: Align) -> Self {
        let top_x = anchor.x
            - match align.h_align {
                HAlign::Left => T::zero(),
                HAlign::Center => size.width / (T::one() + T::one()),
                HAlign::Right => size.width,
            };

        let left_y = anchor.y
            - match align.v_align {
                VAlign::Top => T::zero(),
                VAlign::Middle => size.height / (T::one() + T::one()),
                VAlign::Bottom => size.height,
            };

        Self::new(Point2D::new(top_x, left_y), size)
    }

    fn aligned_inside(&self, container: &Self, align: Align) -> Self {
        self.size.aligned_inside(container, align)
    }
}

pub trait SizeExtra<T, U> {
    fn anchored(&self, anchor: Point2D<T, U>, align: Align) -> Rect<T, U>;
    fn aligned_inside(&self, container: &Rect<T, U>, align: Align) -> Rect<T, U>;
}

impl<T, U> SizeExtra<T, U> for Size2D<T, U>
where
    T: Clone
        + Copy
        + Add<Output = T>
        + Sub<Output = T>
        + Mul<T, Output = T>
        + Div<T, Output = T>
        + PartialOrd
        + Zero
        + One,
{
    fn anchored(&self, anchor: Point2D<T, U>, align: Align) -> Rect<T, U> {
        Rect::from_anchor(anchor, *self, align)
    }

    fn aligned_inside(&self, container: &Rect<T, U>, align: Align) -> Rect<T, U> {
        let anchor = container.anchor(align);
        self.anchored(anchor, align)
    }
}

pub fn barycenter<T, U>(points: impl IntoIterator<Item = Point2D<T, U>>) -> Point2D<T, U>
where
    T: Copy + Zero + One + PartialEq + Div<T, Output = T> + AddAssign + Add<Output = T>,
{
    let mut count = T::zero();
    let mut sum = Vector2D::<T, U>::zero();
    for pt in points {
        count += T::one();
        sum = sum + pt.to_vector();
    }

    if count == T::zero() {
        Point2D::zero()
    } else {
        (sum / count).to_point()
    }
}
