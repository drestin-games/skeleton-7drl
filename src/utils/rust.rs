use std::fmt::Debug;

use crate::console_log;

pub trait DebugFunction<T> {
    fn debug(self, name: &str) -> Self;
    fn debug_map<U: Debug>(self, name: &str, f: impl Fn(&T) -> U) -> Self;
}

impl<T> DebugFunction<T> for T
where
    T: Debug,
{
    fn debug(self, name: &str) -> Self {
        console_log!("{name}: {:?}", &self);
        self
    }

    fn debug_map<U: Debug>(self, name: &str, f: impl Fn(&T) -> U) -> Self {
        console_log!("{name}: {:?}", f(&self));
        self
    }
}
