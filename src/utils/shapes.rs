use super::geometry::{GridPos, GridRect};

pub fn square(center: GridPos, half_side: i32) -> impl Iterator<Item = GridPos> {
    let x_range = (center.x - half_side)..(center.x + half_side + 1);
    let y_range = (center.y - half_side)..(center.y + half_side + 1);

    x_range.zip(y_range).map(|tuple| tuple.into())
}

pub fn rectangle_line(rect: GridRect) -> impl Iterator<Item = GridPos> {
    rectangle_border(rect, 1)
}

pub fn rectangle(rect: GridRect) -> impl Iterator<Item = GridPos> {
    rect.y_range()
        .flat_map(move |y| rect.x_range().map(move |x| (x, y).into()))
}

pub fn rectangle_vec(rect: GridRect) -> Vec<GridPos> {
    let mut vec = Vec::with_capacity((rect.width() * rect.height()) as usize);
    for y in rect.y_range() {
        for x in rect.x_range() {
            vec.push(GridPos::new(x, y));
        }
    }
    vec
}

pub fn rectangle_border(rect: GridRect, thickness: i32) -> Box<dyn Iterator<Item = GridPos>> {
    let min_x = rect.min_x();
    let max_x = rect.max_x() - 1;
    let min_y = rect.min_y();
    let max_y = rect.max_y() - 1;
    let inner_min_x = min_x + thickness - 1;
    let inner_max_x = max_x - thickness + 1;
    let inner_min_y = min_y + thickness - 1;
    let inner_max_y = max_y - thickness + 1;

    if inner_min_x + 1 >= inner_max_x || inner_min_y + 1 >= inner_max_y {
        Box::new(rectangle(rect))
    } else {
        Box::new(
            (min_x..max_x + 1)
                .flat_map(move |x| {
                    (min_y..inner_min_y + 1)
                        .chain(inner_max_y..max_y + 1)
                        .map(move |y| GridPos::new(x, y))
                })
                .chain(((inner_min_y + 1)..inner_max_y).flat_map(move |y| {
                    (min_x..inner_min_x + 1)
                        .chain(inner_max_x..max_x + 1)
                        .map(move |x| GridPos::new(x, y))
                })),
        )
    }
}

// It seems to be 2x faster that the iterator
pub fn rectangle_border_vec(rect: GridRect, thickness: i32) -> Vec<GridPos> {
    let min_x = rect.min_x();
    let max_x = rect.max_x() - 1;
    let min_y = rect.min_y();
    let max_y = rect.max_y() - 1;
    let inner_min_x = min_x + thickness - 1;
    let inner_max_x = max_x - thickness + 1;
    let inner_min_y = min_y + thickness - 1;
    let inner_max_y = max_y - thickness + 1;

    if inner_min_x + 1 >= inner_max_x || inner_min_y + 1 >= inner_max_y {
        rectangle(rect).collect()
    } else {
        let size = rect.width() * rect.height()
            - (rect.width() - 2 * thickness) * (rect.height())
            - 2 * thickness;
        let mut vec = Vec::with_capacity(size as usize);

        vec.extend(
            (min_x..max_x + 1)
                .flat_map(|x| (min_y..inner_min_y + 1).map(move |y| GridPos::new(x, y))),
        );
        vec.extend(
            (min_x..max_x + 1)
                .flat_map(|x| (inner_max_y..max_y + 1).map(move |y| GridPos::new(x, y))),
        );
        vec.extend(
            ((inner_min_y + 1)..inner_max_y)
                .flat_map(|x| (min_x..inner_min_x + 1).map(move |y| GridPos::new(x, y))),
        );
        vec.extend(
            ((inner_min_y + 1)..inner_max_y)
                .flat_map(|x| (inner_max_x..max_x + 1).map(move |y| GridPos::new(x, y))),
        );
        vec
    }
}

pub fn diamond(center: GridPos, radius: i32) -> impl Iterator<Item = GridPos> {
    ((center.x - radius)..(center.x + radius + 1)).flat_map(move |x| {
        let dy = radius - (center.x - x).abs();
        ((center.y - dy)..(center.y + dy + 1)).map(move |y| GridPos::new(x, y))
    })
}

pub fn diamond_ring(
    center: GridPos,
    outer_radius: i32,
    thickness: i32,
) -> impl Iterator<Item = GridPos> {
    ((center.x - outer_radius)..(center.x + outer_radius + 1)).flat_map(move |x| {
        let dy = outer_radius - (center.x - x).abs();
        let min_y = center.y - dy;
        let max_y = center.y + dy;
        let inner_min_y = min_y + thickness - 1;
        let inner_max_y = max_y - thickness + 1;

        let ys = if inner_min_y + 1 >= inner_max_y {
            (min_y..max_y + 1).chain(0..0) // trick to have the same concrete type
        } else {
            (min_y..inner_min_y + 1).chain(inner_max_y..max_y + 1)
        };

        ys.map(move |y| GridPos::new(x, y))
    })
}

pub fn line(a: GridPos, b: GridPos) -> impl Iterator<Item = GridPos> {
    let dx = b.x - a.x;
    let dy = b.y - a.y;
    let n = dx.abs().max(dy.abs());

    let div_n = if n == 0 { 0.0 } else { 1.0 / (n as f32) };
    let x_step = (dx as f32) * div_n;
    let y_step = (dy as f32) * div_n;

    let mut x = a.x as f32;
    let mut y = a.y as f32;

    // RangeInclusive is known to be slow
    // https://www.reddit.com/r/rust/comments/eiwhkn/comment/fctvmt4/
    (0..(n + 1)).map(move |_| {
        let pos = GridPos::new(x.round() as i32, y.round() as i32);
        x += x_step;
        y += y_step;

        pos
    })
}
