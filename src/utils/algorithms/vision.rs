use std::cmp::Ordering;

use crate::utils::{
    geometry::{GridPos, GridRect, GridSize, GridVec},
    grid::Grid,
    shapes::rectangle,
};

#[derive(Clone, Default, Debug)]
pub struct FieldOfView {
    grid: Grid<bool>,
}

impl FieldOfView {
    // Totally copied from the following link. Go there to see explicative comments.
    // http://www.adammil.net/blog/v125_Roguelike_Vision_Algorithms.html#mycode
    pub fn compute(
        origin: GridPos,
        range_limit: i32,
        is_opaque: &impl Fn(GridPos) -> bool,
    ) -> Self {
        let top_left = origin - GridVec::splat(range_limit);
        let rect = GridRect::new(top_left, GridSize::splat(2 * range_limit + 1));

        let mut grid = Grid::filled_rect(rect, false);

        grid.set(origin, true);
        for octant in Octant::ALL {
            compute_octant(
                range_limit,
                1,
                Slope::new(1, 1),
                Slope::new(0, 1),
                &|x, y| GridVec::new(x, y).square_length(),
                &|x, y| is_opaque(octant.transform(x, y, origin)),
                &mut |x, y| {
                    grid.set(octant.transform(x, y, origin), true);
                },
            )
        }

        Self { grid }
    }

    pub fn is_visible(&self, pos: GridPos) -> bool {
        *self.grid.get(pos).unwrap_or(&false)
    }

    pub fn is_visible_rect(&self, rect: GridRect) -> bool {
        rectangle(rect).any(|pos| self.is_visible(pos))
    }
}

#[derive(Clone, Copy, Debug)]
struct Slope {
    x: i32,
    y: i32,
}

/// y / x
impl Slope {
    fn new(y: i32, x: i32) -> Self {
        Self { x, y }
    }
}

impl PartialOrd for Slope {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        let ord = if self == other {
            Ordering::Equal
        } else if self.y * other.x > self.x * other.y {
            Ordering::Greater
        } else {
            Ordering::Less
        };
        Some(ord)
    }
}

impl PartialEq for Slope {
    fn eq(&self, other: &Self) -> bool {
        self.y * other.x == self.x * other.y
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Octant {
    NorthWest,
    NorthEast,
    EastNorth,
    EastSouth,
    SouthEast,
    SouthWest,
    WestSouth,
    WestEast,
}

use Octant::*;

impl Octant {
    pub const ALL: [Self; 8] = [
        NorthWest, NorthEast, EastNorth, EastSouth, SouthEast, SouthWest, WestSouth, WestEast,
    ];
    pub fn transform(&self, x: i32, y: i32, origin: GridPos) -> GridPos {
        let delta = match self {
            NorthWest => (-y, -x),
            NorthEast => (y, -x),
            EastNorth => (x, -y),
            EastSouth => (x, y),
            SouthEast => (y, x),
            SouthWest => (-y, x),
            WestSouth => (-x, y),
            WestEast => (-x, -y),
        };

        origin + GridVec::from(delta)
    }
}

fn compute_octant(
    range_limit: i32,
    start_x: i32,
    mut top: Slope,
    mut bottom: Slope,
    squared_distance: &impl Fn(i32, i32) -> i32,
    is_blocking_light: &impl Fn(i32, i32) -> bool,
    set_visible: &mut impl FnMut(i32, i32),
) {
    for x in start_x..range_limit + 1 {
        let top_y = if top.x == 1 {
            x
        } else {
            let mut top_y = ((x * 2 - 1) * top.y + top.x) / (top.x * 2);

            if is_blocking_light(x, top_y) {
                if (top >= Slope::new(top_y * 2 + 1, x * 2)) && !is_blocking_light(x, top_y + 1) {
                    top_y += 1;
                }
            } else {
                let mut ax = x * 2;
                if is_blocking_light(x + 1, top_y + 1) {
                    ax += 1;
                }
                if top > Slope::new(top_y * 2 + 1, ax) {
                    top_y += 1;
                }
            }

            top_y
        };

        let bottom_y = if bottom.y == 0 {
            0
        } else {
            let mut bottom_y = ((x * 2 - 1) * bottom.y + bottom.x) / (bottom.x * 2);

            if bottom >= Slope::new(bottom_y * 2 + 1, x * 2)
                && is_blocking_light(x, bottom_y)
                && !is_blocking_light(x, bottom_y + 1)
            {
                bottom_y += 1;
            }

            bottom_y
        };

        let mut was_opaque: Option<bool> = None;
        for y in (bottom_y..top_y + 1).rev() {
            if squared_distance(x, y) <= range_limit * range_limit {
                let is_opaque = is_blocking_light(x, y);
                let is_visible = is_opaque
                    || ((y != top_y || top >= Slope::new(y, x))
                        && (y != bottom_y || bottom <= Slope::new(y, x)));
                if is_visible {
                    set_visible(x, y);
                }

                if x != range_limit {
                    if is_opaque {
                        if was_opaque == Some(false) {
                            let mut nx = x * 2;
                            let ny = y * 2 + 1;

                            if is_blocking_light(x, y + 1) {
                                nx -= 1;
                            }

                            if top > Slope::new(ny, nx) {
                                if y == bottom_y {
                                    bottom = Slope::new(ny, nx);
                                    break;
                                } else {
                                    compute_octant(
                                        range_limit,
                                        x + 1,
                                        top,
                                        Slope::new(ny, nx),
                                        squared_distance,
                                        is_blocking_light,
                                        set_visible,
                                    )
                                }
                            } else if y == bottom_y {
                                return;
                            }
                        }
                        was_opaque = Some(true);
                    } else {
                        if was_opaque == Some(true) {
                            let mut nx = x * 2;
                            let ny = y * 2 + 1;

                            if is_blocking_light(x + 1, y + 1) {
                                nx += 1;
                            }

                            if bottom >= Slope::new(ny, nx) {
                                return;
                            }
                            top = Slope::new(ny, nx);
                        }
                        was_opaque = Some(false);
                    }
                }
            }
        }
        if was_opaque != Some(false) {
            return;
        }
    }
}
