use std::collections::{BTreeMap, HashSet, VecDeque};

use crate::utils::geometry::{Corner, GridConnection, GridPos, OrientedEdge};

pub fn _flood_fill(
    connection_type: GridConnection,
    start: GridPos,
    is_in_area: impl Fn(GridPos) -> bool,
) -> HashSet<GridPos> {
    if !is_in_area(start) {
        return HashSet::new();
    }

    let mut already_seen = HashSet::new();
    let mut queue = VecDeque::new();
    let mut in_area = HashSet::new();

    already_seen.insert(start);
    queue.push_back(start);
    in_area.insert(start);

    while let Some(current) = queue.pop_front() {
        for pos in connection_type.neighbours(current) {
            already_seen.insert(pos);
            if is_in_area(pos) {
                in_area.insert(pos);
                queue.push_back(pos);
            }
        }
    }

    in_area
}

pub fn _inner_perimeter(
    connection_type: GridConnection,
    start: GridPos,
    is_in_area: impl Fn(GridPos) -> bool,
) -> HashSet<GridPos> {
    if !is_in_area(start) {
        return HashSet::new();
    }

    let mut already_seen = HashSet::new();
    let mut queue = VecDeque::new();
    let mut perimeter = HashSet::new();

    already_seen.insert(start);
    queue.push_back(start);

    while let Some(current) = queue.pop_front() {
        for pos in connection_type.neighbours(current) {
            already_seen.insert(pos);
            if is_in_area(pos) {
                queue.push_back(pos);
            } else {
                perimeter.insert(current);
            }
        }
    }

    perimeter
}

pub fn _outside_perimeter(
    connection_type: GridConnection,
    start: GridPos,
    is_in_area: impl Fn(GridPos) -> bool,
) -> HashSet<GridPos> {
    if !is_in_area(start) {
        return HashSet::new();
    }

    let mut already_seen = HashSet::new();
    let mut queue = VecDeque::new();
    let mut perimeter = HashSet::new();

    already_seen.insert(start);
    queue.push_back(start);

    while let Some(current) = queue.pop_front() {
        for pos in connection_type.neighbours(current) {
            already_seen.insert(pos);
            if is_in_area(pos) {
                queue.push_back(pos);
            } else {
                perimeter.insert(pos);
            }
        }
    }

    perimeter
}

pub fn perimeter_edges(area: &HashSet<GridPos>) -> HashSet<OrientedEdge> {
    let mut frontiers = HashSet::new();

    for &pos in area {
        for neighbours in GridConnection::Four.neighbours(pos) {
            if !area.contains(&neighbours) {
                frontiers.insert(OrientedEdge::between(pos, neighbours).unwrap());
            }
        }
    }

    frontiers
}

// The order is not garanted to be the same each time if different
// parts of the area are touching at a corner.
// => don't use it with do draw dashed lines.
pub fn perimeter_shapes(area: &HashSet<GridPos>) -> Vec<Vec<Corner>> {
    let edges = perimeter_edges(area);
    let mut map1 = BTreeMap::new();
    let mut map2 = BTreeMap::new();

    for edge in edges {
        let (start, end) = edge.corners();
        // Can appear twice. Ex: for the area : [(0,0);(1,1)]
        if !map1.contains_key(&start) {
            map1.insert(start, end);
        } else {
            map2.insert(start, end);
        }
    }
    let mut shapes = Vec::new();
    while let Some((&start, _)) = map1.first_key_value().or_else(|| map2.first_key_value()) {
        let mut shape: Vec<Corner> = Vec::new();
        let mut elem = start;
        shape.push(elem);

        while let Some(next) = map1.remove(&elem).or_else(|| map2.remove(&elem)) {
            shape.push(next);
            elem = next;
        }

        assert!(shape.len() >= 5 && shape.len() % 2 == 1);
        assert!(shape.first() == shape.last());
        shapes.push(shape);
    }

    shapes
}
