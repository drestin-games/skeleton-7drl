use crate::utils::{
    geometry::{GridConnection, GridPos},
    grid::Grid,
    rng::MyRng,
};

pub struct CellularAutomataSpec<T> {
    pub rule_if_empty: Rule,
    pub rule_if_full: Rule,
    pub empty: T,
    pub full: T,
}

impl<T> CellularAutomataSpec<T>
where
    T: Clone + PartialEq,
{
    pub fn randomize(&self, grid: &mut Grid<T>, proba_full: f64, rng: &mut MyRng) {
        for (_, cell) in grid.iter_mut() {
            let roll = rng.f64(0.0, 1.0);
            *cell = if roll < proba_full {
                self.full.clone()
            } else {
                self.empty.clone()
            };
        }
    }

    pub fn generate_random_pick(
        &mut self,
        grid: &mut Grid<T>,
        nb_iterations: usize,
        rng: &mut MyRng,
    ) {
        for _ in 0..nb_iterations {
            let x = rng.i32(0..grid.width());
            let y = rng.i32(0..grid.height());
            let pos = GridPos::new(x, y);

            let new_value = self.resolve_cell(grid, pos);
            grid.set(pos, new_value);
        }
    }

    pub fn generate_generation_by_generation(&mut self, grid: &mut Grid<T>, nb_generations: usize) {
        let mut next_grid = grid.clone();
        for _ in 0..nb_generations {
            for y in 0..grid.height() {
                for x in 0..grid.width() {
                    let pos = GridPos::new(x, y);
                    let new_value = self.resolve_cell(grid, pos);
                    next_grid.set(pos, new_value);
                }
            }
            std::mem::swap(grid, &mut next_grid);
        }
    }

    fn resolve_cell(&self, grid: &mut Grid<T>, pos: GridPos) -> T {
        let n = self.count_full_neighbours(pos, grid);

        let rule = if self.is_cell_full(pos, grid) {
            self.rule_if_full
        } else {
            self.rule_if_empty
        };

        if rule.bit(n) {
            self.full.clone()
        } else {
            self.empty.clone()
        }
    }

    fn count_full_neighbours(&self, pos: GridPos, grid: &mut Grid<T>) -> usize {
        GridConnection::Eight
            .neighbours(pos)
            .filter(|p| self.is_cell_full(*p, grid))
            .count()
    }

    fn is_cell_full(&self, pos: GridPos, grid: &mut Grid<T>) -> bool {
        grid.get(pos).unwrap_or(&self.full) == &self.full
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
/// Nine bit flag, where a one at index n means that
/// the cell will be full next generation if it has n full neighbours,
/// empty otherwise.
///
/// Usage: Rules(0b111_100_000) // full for n >= 4;
pub struct Rule(pub u16);

impl Rule {
    pub fn bit(&self, n: usize) -> bool {
        debug_assert!(n <= 9);
        (self.0 & (1 << n)) != 0
    }
}
