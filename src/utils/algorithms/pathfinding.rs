use std::{
    cmp::Reverse,
    collections::{HashMap, VecDeque},
};

use priority_queue::PriorityQueue;

use crate::{
    constants::{DIAGONAL_MOVE_COST, MOVEMENT_TYPE, ORTHOGAL_MOVE_COST},
    utils::geometry::GridPos,
};

/// Return the path start excluded, goal included.
pub fn find_shortest_path(
    start: GridPos,
    one_goal: GridPos,
    is_goal: &impl Fn(GridPos) -> bool,
    is_walkable: &impl Fn(GridPos) -> bool,
) -> Option<VecDeque<GridPos>> {
    // A*
    let mut open: PriorityQueue<GridPos, Reverse<i32>> = PriorityQueue::new();
    let mut came_from = HashMap::new();

    let mut g_score: HashMap<GridPos, i32> = HashMap::new();
    g_score.insert(start, 0);

    open.push(start, Reverse(heuristic(start, one_goal)));

    while let Some((current, _)) = open.pop() {
        if is_goal(current) {
            // reconstruct path
            let mut path = VecDeque::new();
            let mut backward_current = current;
            while backward_current != start {
                path.push_front(backward_current);
                backward_current = *came_from
                    .get(&backward_current)
                    .expect("Could not reconstruct the path");
            }
            return Some(path);
        }

        for (neighbour, cost) in MOVEMENT_TYPE
            .neighbours_with_cost(current)
            .filter(|&(pos, _)| is_walkable(pos) || is_goal(pos))
        {
            let tentative_g_score = g_score.get(&current).unwrap_or(&i32::MAX) + cost;
            let neighbour_g_score = g_score.entry(neighbour).or_insert(i32::MAX);
            if tentative_g_score < *neighbour_g_score {
                *neighbour_g_score = tentative_g_score;
                let priority = tentative_g_score + heuristic(neighbour, one_goal);
                open.push(neighbour, Reverse(priority));
                came_from.insert(neighbour, current);
            }
        }
    }

    // failed to find a path
    None
}

fn heuristic(from: GridPos, to: GridPos) -> i32 {
    let dx = (from.x - to.x).abs();
    let dy = (from.y - to.y).abs();
    let diff = (dx - dy).abs();

    let diag_count = dx.max(dy) - diff;
    let ortho_count = diff;

    diag_count * DIAGONAL_MOVE_COST + ortho_count * ORTHOGAL_MOVE_COST
}
