use std::{
    cmp::Reverse,
    collections::{HashSet, VecDeque},
    hash::Hash,
};

use euclid::Rect;
use priority_queue::PriorityQueue;

use crate::{
    console_error, console_log,
    utils::{
        geometry::{Direction, GridConnection, GridPos, GridRect, GridSize},
        grid::Grid,
        rng::{Choosable, MyRng},
    },
};

#[derive(Default)]
pub struct WfcOptions {}

pub struct WaveFunctionCollapse<T> {
    rect: GridRect,
    oracle: CompatibilityOracle<T>,
    possibilities: HashSet<T>,
    _opts: WfcOptions,
}

#[derive(Clone, Debug)]
enum CellState<T> {
    Collapsed(T),
    Quantum(HashSet<T>),
}

impl<T> CellState<T> {
    fn _is_collapsed(&self) -> bool {
        matches!(self, Collapsed(_))
    }

    fn _possibilities_count(&self) -> usize {
        match self {
            Collapsed(_) => 1,
            Quantum(set) => set.len(),
        }
    }

    fn _is_contradiction(&self) -> bool {
        self._possibilities_count() == 0
    }

    fn possibilities(&self) -> HashSet<T>
    where
        T: Copy + Hash + Eq,
    {
        match self {
            Collapsed(val) => HashSet::from([*val]),
            Quantum(set) => set.clone(),
        }
    }
}

use CellState::*;

impl<T> WaveFunctionCollapse<T>
where
    T: Copy + Hash + Eq,
{
    pub fn new(input: Grid<T>, size: GridSize) -> Self {
        let mut possibilities: HashSet<_> = HashSet::new();

        input.iter().for_each(|(_, t)| {
            if !possibilities.contains(t) {
                possibilities.insert(t.clone());
            }
        });

        let oracle = CompatibilityOracle::new(input);

        Self {
            rect: Rect::from_size(size),
            oracle,
            possibilities,
            _opts: Default::default(),
        }
    }

    pub fn collapse(&self, rng: &mut MyRng) -> Grid<T> {
        let mut n = 0;
        loop {
            if n > 1000 {
                console_error!("Failed to generate map with WFC");
                let tile = self.possibilities.iter().collect::<Vec<_>>()[0];
                return Grid::filled_rect(self.rect, *tile);
            }
            n += 1;
            let instance = WfcInstance::new(self.rect, self.possibilities.clone());
            if let Ok(grid) = instance.collapse(&self.oracle, rng) {
                console_log!("WFC successfully collapsed after {n} tries.");
                return grid;
            } else {
                continue;
            }
        }
    }
}

struct ContradictionError;

struct WfcInstance<T> {
    queue: PriorityQueue<GridPos, Reverse<usize>>,
    grid: Grid<CellState<T>>,
}

impl<T: Copy + Hash + Eq> WfcInstance<T> {
    fn new(rect: GridRect, possibilities: HashSet<T>) -> Self {
        let grid = Grid::filled_rect(rect, Quantum(possibilities.clone()));
        let queue = PriorityQueue::new();

        Self { queue, grid }
    }

    pub fn collapse(
        mut self,
        oracle: &CompatibilityOracle<T>,
        rng: &mut MyRng,
    ) -> Result<Grid<T>, ContradictionError> {
        let start = rng.pos(self.grid.rect().min(), self.grid.rect().max());
        self.queue.push(start, Reverse(0));

        while let Some(pos) = self.find_next_cell_to_collapse() {
            let cell = self.grid.get_mut(pos).unwrap();
            let chosen = match cell {
                Collapsed(_) => unreachable!("This cell should not have been chosen"),
                Quantum(possibilities) => {
                    if possibilities.is_empty() {
                        return Err(ContradictionError);
                    } else {
                        possibilities
                            .iter()
                            .choose(rng)
                            .expect("not empty => can't be None")
                    }
                }
            };
            *cell = Collapsed(chosen.clone());

            self.propagate(pos, oracle, rng);
        }

        let result_grid = Grid::from_fn_rect(self.grid.rect(), |pos| match self.grid.get(pos) {
            Some(Collapsed(t)) => t.clone(),
            _ => unreachable!("The WFC is supposed to be totally collapsed"),
        });

        Ok(result_grid)
    }

    fn find_next_cell_to_collapse(&mut self) -> Option<GridPos> {
        self.queue.pop().map(|(pos, _)| pos)
    }

    fn _is_totally_collapsed(&self) -> bool {
        self.grid.iter().all(|(_, c)| c._is_collapsed())
    }

    fn propagate(
        &mut self,
        changed_pos: GridPos,
        oracle: &CompatibilityOracle<T>,
        rng: &mut MyRng,
    ) {
        let mut queue = VecDeque::new();

        queue.push_back(changed_pos);

        while let Some(current_pos) = queue.pop_front() {
            let current_possibilities = self.grid.get(current_pos).unwrap().possibilities();

            for (other_pos, dir) in GridConnection::Four.neighbours_with_dir(current_pos) {
                if let Some(CellState::Quantum(possibilities)) = self.grid.get_mut(other_pos) {
                    let mut changed = false;
                    for other_tile in possibilities.clone() {
                        if !current_possibilities.iter().any(|current_tile| {
                            oracle.is_compatible(*current_tile, other_tile, dir)
                        }) {
                            possibilities.remove(&other_tile);
                            changed = true;
                        }
                    }

                    if changed {
                        queue.push_back(other_pos);
                    }

                    if changed || self.queue.get_priority(&other_pos).is_none() {
                        let priority = Self::calculate_priority(&possibilities, rng);
                        self.queue.push(other_pos, Reverse(priority));
                    }
                }
            }
        }
    }

    fn calculate_priority(set: &HashSet<T>, rng: &mut MyRng) -> usize {
        let noise_range = 1000;
        set.len() * noise_range + rng.usize(0..noise_range)
    }
}

type Compatibility<T> = (T, T, Direction);

struct CompatibilityOracle<T> {
    compatibilities: HashSet<Compatibility<T>>,
}

impl<T: Eq + Hash + Copy> CompatibilityOracle<T> {
    fn new(input: Grid<T>) -> Self {
        let mut compatibilities = HashSet::new();

        for (pos, tile) in input.iter() {
            for (other_pos, dir) in GridConnection::Four.neighbours_with_dir(pos) {
                if let Some(other_tile) = input.get(other_pos) {
                    compatibilities.insert((*tile, *other_tile, dir));
                }
            }
        }

        Self { compatibilities }
    }

    fn is_compatible(&self, a: T, b: T, dir: Direction) -> bool {
        self.compatibilities.contains(&(a, b, dir))
    }
}
