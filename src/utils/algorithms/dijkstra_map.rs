use std::{
    cmp::Reverse,
    ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Sub, SubAssign},
};

use priority_queue::PriorityQueue;

use crate::{
    constants::MOVEMENT_TYPE,
    utils::{
        geometry::{Direction, GridPos, GridRect},
        grid::Grid,
    },
};

#[derive(Default, Clone)]
pub struct DijkstraMap {
    grid: Grid<Option<i32>>,
}

impl DijkstraMap {
    pub fn _compute(
        rect: GridRect,
        goals: impl IntoIterator<Item = GridPos>,
        is_walkable: &impl Fn(GridPos) -> bool,
    ) -> Self {
        let mut grid = Grid::filled_rect(rect, None);

        let mut open: PriorityQueue<GridPos, Reverse<i32>> = PriorityQueue::new();
        for goal in goals {
            open.push(goal, Reverse(0));
        }

        while let Some((current, Reverse(value))) = open.pop() {
            grid.set(current, Some(value));

            for (neighbour, cost) in MOVEMENT_TYPE
                .neighbours_with_cost(current)
                .filter(|&(pos, _)| grid.get(pos) == Some(&None) && is_walkable(pos))
            {
                let new_value = value + cost;
                if open
                    .get_priority(&neighbour)
                    .filter(|Reverse(v)| new_value >= *v)
                    .is_none()
                {
                    open.push(neighbour, Reverse(new_value));
                }
            }
        }

        Self { grid }
    }

    pub fn _best_dirs_from(&self, pos: GridPos) -> Option<impl Iterator<Item = Direction> + '_> {
        if !self.grid._contains(pos) {
            return None;
        }

        Direction::ALL_DIR_8
            .iter()
            .filter_map(|dir| self.get(pos + dir.delta_vec()))
            .min()
            .map(|min| {
                Direction::ALL_DIR_8
                    .iter()
                    .filter(move |dir| self.get(pos + dir.delta_vec()) == Some(min))
                    .copied()
            })
    }

    pub fn get(&self, pos: GridPos) -> Option<i32> {
        self.grid.get(pos).copied().flatten()
    }

    pub fn _max(&self) -> Option<i32> {
        self.grid.iter().filter_map(|(_, v)| *v).max()
    }

    pub fn _min(&self) -> Option<i32> {
        self.grid.iter().filter_map(|(_, v)| *v).min()
    }

    fn op_assign_map(&mut self, other: &DijkstraMap, mut op: impl FnMut(&mut i32, i32) -> ()) {
        self.grid.iter_mut().for_each(|(pos, val)| {
            let other_val = other.get(pos);
            if let (Some(val), Some(other_val)) = (val, other_val) {
                op(val, other_val);
            }
        })
    }

    fn op_assign(&mut self, mut op: impl FnMut(&mut i32) -> ()) {
        self.grid.iter_mut().for_each(|(_, val)| {
            if let Some(val) = val {
                op(val);
            }
        })
    }
}

impl AddAssign<&DijkstraMap> for DijkstraMap {
    fn add_assign(&mut self, other: &DijkstraMap) {
        self.op_assign_map(other, |val, other_val| *val += other_val);
    }
}

impl SubAssign<&DijkstraMap> for DijkstraMap {
    fn sub_assign(&mut self, other: &DijkstraMap) {
        self.op_assign_map(other, |val, other_val| *val -= other_val);
    }
}

impl AddAssign<i32> for DijkstraMap {
    fn add_assign(&mut self, n: i32) {
        self.op_assign(|val| *val += n);
    }
}

impl SubAssign<i32> for DijkstraMap {
    fn sub_assign(&mut self, n: i32) {
        self.op_assign(|val| *val -= n);
    }
}

impl MulAssign<i32> for DijkstraMap {
    fn mul_assign(&mut self, n: i32) {
        self.op_assign(|val| *val *= n);
    }
}

impl DivAssign<i32> for DijkstraMap {
    fn div_assign(&mut self, n: i32) {
        self.op_assign(|val| *val /= n);
    }
}

impl Add<i32> for DijkstraMap {
    type Output = Self;

    fn add(mut self, n: i32) -> Self::Output {
        self += n;
        self
    }
}

impl Sub<i32> for DijkstraMap {
    type Output = Self;

    fn sub(mut self, n: i32) -> Self::Output {
        self -= n;
        self
    }
}

impl Mul<i32> for DijkstraMap {
    type Output = Self;

    fn mul(mut self, n: i32) -> Self::Output {
        self *= n;
        self
    }
}

impl Div<i32> for DijkstraMap {
    type Output = Self;

    fn div(mut self, n: i32) -> Self::Output {
        self /= n;
        self
    }
}
