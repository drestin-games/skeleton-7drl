#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Rank(u32);

impl Rank {
    pub fn is_next_of(&self, other: Self) -> bool {
        self.0 - other.0 == 1
    }
}

#[derive(Debug, Clone)]
pub struct RankGenerator {
    next_rank: u32,
}

impl RankGenerator {
    pub fn new(start: u32) -> Self {
        Self { next_rank: start }
    }

    pub fn generate(&mut self) -> Rank {
        let rank = Rank(self.next_rank);
        self.next_rank += 1;
        rank
    }
}
