use crate::{
    constants::{MOVEMENT_TYPE, PLAYER_VISION_RANGE},
    ui::{
        color::Color,
        colored_text::{Colorable, ColoredText},
    },
    utils::{
        algorithms::{
            cellular_automata::{CellularAutomataSpec, Rule},
            vision::FieldOfView,
        },
        geometry::{Direction, GridPos, GridRect, GridSize},
        grid::Grid,
        id::ActorId,
        shapes::{line, rectangle, rectangle_line},
    },
};
use std::collections::BTreeMap;

use super::super::screen::ServerOnlyData;

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum Tile {
    Unreachable,
    Wall,
    Ground,
    Stairs,
}

use Tile::*;
impl Tile {
    pub fn is_opaque(&self) -> bool {
        match self {
            Wall | Unreachable => true,
            Ground | Stairs => false,
        }
    }

    pub fn is_walkable(&self) -> bool {
        match self {
            Wall | Unreachable => false,
            Ground | Stairs => true,
        }
    }

    pub fn symbol(&self) -> char {
        match self {
            Wall => '#',
            Ground => '.',
            Stairs => '>',
            Unreachable => ' ',
        }
    }

    pub fn color(&self) -> Color {
        match self {
            Wall | Stairs | Unreachable => Color::WHITE,
            Ground => Color::DARK_GREY,
        }
    }
}

#[derive(Clone, Debug)]
pub struct GameState {
    pub player_vision: FieldOfView,
    map: Grid<Tile>,
    player_id: ActorId,
    pub actors: BTreeMap<ActorId, Actor>,
}

impl GameState {
    pub fn new(player: Actor, server_data: &mut ServerOnlyData) -> Self {
        let side = 20;
        let width = side;
        let height = side;
        let mut map = Grid::filled(width, height, Tile::Ground);

        let mut cellular_automata = CellularAutomataSpec {
            rule_if_empty: Rule(0b111_110_000),
            rule_if_full: Rule(0b111_100_000),
            empty: Tile::Ground,
            full: Tile::Wall,
        };
        cellular_automata.randomize(&mut map, 0.5, &mut server_data.rng);
        rectangle(GridRect::new(GridPos::new(5, 5), GridSize::new(10, 10)))
            .for_each(|pos| map.set(pos, Tile::Ground));
        cellular_automata.generate_generation_by_generation(&mut map, 10);

        map.set(GridPos::new(1, 3), Tile::Stairs);
        line(GridPos::new(10, 10), GridPos::new(7, 9)).for_each(|pos| map.set(pos, Tile::Wall));
        let mut state = Self {
            player_vision: FieldOfView::default(),
            player_id: player.id,
            map,
            actors: BTreeMap::new(),
        };
        // rectangle(GridRect::from_size(GridSize::new(width, height)))
        rectangle_line(GridRect::from_size(GridSize::new(
            width as i32,
            height as i32,
        )))
        .for_each(|pos| state.map.set(pos, Tile::Wall));
        state.actors.insert(player.id, player);

        for i in 2..=4 {
            let actor_id = server_data.id_generator.generate();
            state
                .actors
                .insert(actor_id, Actor::new_enemy(actor_id, GridPos::splat(i * 2)));
        }

        state.compute_player_vision();

        state
    }

    pub fn get_player(&self) -> &Actor {
        self.actors.get(&self.player_id).unwrap_or_else(|| {
            panic!("Could not find the player!");
        })
    }

    pub fn get_actor_or_panic(&self, actor_id: ActorId) -> &Actor {
        self.actors.get(&actor_id).unwrap_or_else(|| {
            panic!("Could not find actor with id {actor_id:?}!");
        })
    }

    pub fn _can_actor_move_dir(&self, actor: &Actor, dir: Direction) -> bool {
        let new_pos = actor.pos + dir.delta_vec();
        actor
            ._occupied_tiles_at_pos(new_pos)
            .map(|pos| self.tile_at(pos))
            .all(|tile| tile.is_walkable())
    }

    pub fn action_from_dir_input(&self, actor: &Actor, dir: Direction) -> Option<DirAction> {
        let new_pos = actor.pos + dir.delta_vec();

        self.action_from_move_target(actor, new_pos)
    }

    pub fn action_from_move_target(&self, actor: &Actor, new_pos: GridPos) -> Option<DirAction> {
        if !MOVEMENT_TYPE.are_neighbours(new_pos, actor.pos) {
            return None;
        }

        let new_rect = actor.rect_at_pos(new_pos);

        let mut targets = Vec::new();
        let mut blocked = false;
        for target in self
            .actors
            .values()
            .filter(|a| a.id != actor.id && new_rect.intersects(&a.rect()))
        {
            blocked = true;
            if target.team != actor.team {
                targets.push(target.id);
            }
        }

        if !targets.is_empty() {
            Some(DirAction::Hit(targets))
        } else if !blocked
            && rectangle(new_rect)
                .map(|pos| self.tile_at(pos))
                .all(|tile| tile.is_walkable())
        {
            Some(DirAction::Move(new_pos))
        } else {
            None
        }
    }

    pub fn compute_player_vision(&mut self) {
        self.player_vision =
            FieldOfView::compute(self.get_player().pos, PLAYER_VISION_RANGE, &|pos| {
                self.tile_at(pos).is_opaque()
            });
    }

    pub fn tile_at(&self, pos: GridPos) -> Tile {
        *self.map.get(pos).unwrap_or(&Tile::Unreachable)
    }

    pub fn can_player_descend_stairs(&self) -> bool {
        self.tile_at(self.get_player().pos) == Tile::Stairs
    }

    pub fn is_player(&self, actor_id: ActorId) -> bool {
        actor_id == self.player_id
    }
}

#[derive(Debug, Clone)]
pub struct Actor {
    pub id: ActorId,
    pub name: String,
    pub pos: GridPos,
    pub health: i32,
    pub team: Team,
    pub size: i32,
    pub symbol: char,
    pub color: Color,
}

impl Actor {
    pub fn new_player(id: ActorId, pos: GridPos) -> Self {
        Self {
            id,
            name: "Player".into(),
            pos,
            health: 1000,
            team: Team::Ally,
            size: 1,
            symbol: '@',
            color: Color::WHITE,
        }
    }

    pub fn new_enemy(id: ActorId, pos: GridPos) -> Self {
        Self {
            id,
            name: "Enemy".into(),
            pos,
            health: 4,
            team: Team::Enemy,
            size: 1,
            symbol: 'b',
            color: Color::BLUE,
        }
    }

    pub fn rect(&self) -> GridRect {
        GridRect::new(self.pos, GridSize::splat(self.size))
    }

    pub fn rect_at_pos(&self, pos: GridPos) -> GridRect {
        GridRect::new(pos, GridSize::splat(self.size))
    }

    pub fn colored_name(&self) -> ColoredText {
        self.name.colored(self.color)
    }

    pub fn occupied_tiles(&self) -> impl Iterator<Item = GridPos> {
        rectangle(self.rect())
    }

    pub fn _occupied_tiles_at_pos(&self, pos: GridPos) -> impl Iterator<Item = GridPos> {
        rectangle(self.rect_at_pos(pos))
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Team {
    Ally,
    Enemy,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum DirAction {
    Move(GridPos),
    Hit(Vec<ActorId>),
}
