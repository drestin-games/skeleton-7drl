use std::ops::Deref;

use crate::{
    constants::MOVEMENT_TYPE,
    gameplay::{
        back::event::{descend_stairs_event::descend_stairs, turn_event::handle_move_input},
        command::command::Command,
        screen::ServerOnlyData,
        state::game_state::{Actor, GameState},
    },
    utils::geometry::GridPos,
};

use super::event::game_event::{BoxedAnyEventNode, BoxedChildrenOf, Event, EventNode};

pub struct BackSide {
    pub(super) game: GameWrapper,
    pub(super) server_data: ServerOnlyData,
}

impl BackSide {
    pub fn with_seed(seed: u64) -> Self {
        let mut server_data = ServerOnlyData::with_rng_seed(seed);
        let player = Actor::new_player(server_data.id_generator.generate(), GridPos::new(10, 10));
        let game = GameState::new(player, &mut server_data);
        let game = GameWrapper(game);

        Self { game, server_data }
    }

    pub fn handle_command(&mut self, command: Command) -> Option<BoxedAnyEventNode> {
        use Command::*;
        match command {
            Move(dir) if MOVEMENT_TYPE.is_valid_dir(dir) => {
                handle_move_input(self, dir).map(|n| n._erase_type())
            }
            DescendStairs if self.game.can_player_descend_stairs() => Some(descend_stairs(self)),
            _ => None,
        }
    }

    pub fn current_game_state(&self) -> &GameState {
        &self.game
    }

    pub fn execute_leaf_event<E>(&mut self, event: E) -> Box<EventNode<E>>
    where
        E: Clone + Event<Children = ()> + 'static,
    {
        self.execute_event_with_consequences(event, |_| ())
    }

    pub fn execute_event_with_consequences<E>(
        &mut self,
        event: E,
        f: impl FnOnce(&mut Self) -> BoxedChildrenOf<E>,
    ) -> Box<EventNode<E>>
    where
        E: Clone + Event + 'static,
    {
        let rank = self.server_data.event_rank_generator.generate();
        let event = Box::new(event);
        event.clone().apply(self.game.mutable_access());
        let consequences = f(self);
        Box::new(EventNode::new(rank, event, consequences))
    }
}

/// A wrapper that only implement Deref, and not DerefMut.
/// To prevent mutations to the game state outside of apply_and_save_event().
pub(super) struct GameWrapper(GameState);

impl Deref for GameWrapper {
    type Target = GameState;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl GameWrapper {
    pub fn mutable_access(&mut self) -> &mut GameState {
        &mut self.0
    }
}
