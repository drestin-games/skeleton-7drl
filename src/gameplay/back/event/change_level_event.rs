use std::time::Duration;

use crate::{
    constants::{BACKGROUND_COLOR, SCREEN_FADE_DURATION},
    gameplay::{
        back::back::BackSide,
        front::animation::animation::{AnimFunction, GeneralAnimation},
        state::game_state::GameState,
    },
    utils::animated::{Animated, Constant, Easing, Interpolation},
};

use super::game_event::{BoxedEventNode, Event};

#[derive(Debug, Clone)]
pub struct ChangeLevelEvent {
    pub new_game_state: GameState,
}

impl Event for ChangeLevelEvent {
    type Children = ();
    fn apply(self: Box<Self>, game: &mut GameState) {
        *game = self.new_game_state;
    }

    fn animate_self(self: Box<Self>) -> GeneralAnimation
    where
        Self: 'static,
    {
        let fade_in = Interpolation::zero_one(SCREEN_FADE_DURATION, Easing::Linear);
        let between = Constant::new(1.0, Duration::from_millis(200));
        let fade_out = fade_in.clone().rev();
        let animated = fade_in.chain(between).chain(fade_out);
        let duration = animated.duration();
        let f: AnimFunction = Box::new(move |p| {
            let opacity = animated.get(p.elapsed);
            p.overrides.screen_fade = Some((BACKGROUND_COLOR, opacity));
        });
        GeneralAnimation::simple(self, duration, f).with_apply_event_at(duration / 2)
    }
}
pub fn change_level(
    b: &mut BackSide,
    new_game_state: GameState,
) -> BoxedEventNode<ChangeLevelEvent> {
    let event = ChangeLevelEvent { new_game_state };
    b.execute_leaf_event(event)
}
