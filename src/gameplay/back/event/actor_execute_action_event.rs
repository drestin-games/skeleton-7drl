use crate::{
    gameplay::{
        back::{back::BackSide, event::game_event::Event},
        state::game_state::DirAction,
    },
    utils::{geometry::Direction, id::ActorId},
};

use super::{
    actor_hit_event::actor_hit, actor_move_event::actor_move, game_event::BoxedAnyEventNode,
};

#[derive(Clone, Debug)]
pub struct ActorExecuteActionEvent {
    _actor_id: ActorId,
    _action: DirAction,
    _dir: Direction,
}

impl Event for ActorExecuteActionEvent {}

pub fn actor_execute_action(
    b: &mut BackSide,
    actor_id: ActorId,
    action: DirAction,
    dir: Direction,
) -> BoxedAnyEventNode {
    let event = ActorExecuteActionEvent {
        _action: action.clone(),
        _actor_id: actor_id,
        _dir: dir,
    };
    b.execute_event_with_consequences(event, |b| match action {
        DirAction::Move(new_pos) => vec![actor_move(b, actor_id, new_pos)],
        DirAction::Hit(targets) => vec![actor_hit(b, actor_id, targets, dir)],
    })
}
