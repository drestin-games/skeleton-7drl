use crate::{
    console_warn,
    constants::MOVE_DURATION,
    gameplay::{
        back::back::BackSide,
        front::animation::animation::{AnimFunction, Flags, GeneralAnimation},
        state::game_state::GameState,
    },
    utils::{
        animated::{Animated, Easing, Interpolation},
        geometry::{GridPos, WorldVec},
        id::ActorId,
    },
};

use super::game_event::{BoxedEventNode, Event};

#[derive(Debug, Clone)]
pub struct ActorMoveEvent {
    pub actor_id: ActorId,
    pub from: GridPos,
    pub to: GridPos,
}

impl Event for ActorMoveEvent {
    type Children = ();

    fn apply(self: Box<Self>, game: &mut GameState) {
        if let Some(actor) = game.actors.get_mut(&self.actor_id) {
            if actor.pos != self.from {
                console_warn!(
                    "Move: the starting position ({:?}) isn't the one of the event ({:?})",
                    actor.pos,
                    self.from
                );
            }

            actor.pos = self.to;
            if game.is_player(self.actor_id) {
                game.compute_player_vision();
            }
        } else {
            console_warn!("Move: Could not find actor {:?}", self.actor_id);
        }
    }

    fn animate_self(self: Box<Self>) -> GeneralAnimation {
        let duration = MOVE_DURATION;
        let animated_pos =
            Interpolation::new(self.from.cast(), self.to.cast(), duration, Easing::Linear);

        let actor_id = self.actor_id;
        let f: AnimFunction = Box::new(move |p| {
            let rendered_pos = animated_pos.get(p.elapsed);
            p.overrides
                .overriden_positions
                .insert(actor_id, rendered_pos);
            if p.game.is_player(actor_id) {
                p.overrides.camera_center = Some(rendered_pos + WorldVec::splat(0.5));
            }
        });
        let anim = GeneralAnimation::simple(self, duration, f).with_flags(Flags::MOVE);

        anim
    }
}

pub fn actor_move(
    b: &mut BackSide,
    actor_id: ActorId,
    to: GridPos,
) -> BoxedEventNode<ActorMoveEvent> {
    let from = b.game.get_actor_or_panic(actor_id).pos;
    let event = ActorMoveEvent { actor_id, from, to };
    b.execute_leaf_event(event)
}
