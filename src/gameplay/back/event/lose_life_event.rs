use crate::{
    constants::HP_LOST_TEXT_COLOR,
    gameplay::{
        back::back::BackSide,
        front::animation::animation::{flash_tiles, floating_text, GeneralAnimation},
        state::game_state::GameState,
    },
    ui::colored_text::{Colorable, ColoredText},
    utils::{
        geometry::{barycenter, GridPos, WorldPoint},
        id::ActorId,
    },
};

use super::game_event::{BoxedEventNode, Event};

#[derive(Debug, Clone)]
pub struct LoseLifeEvent {
    actor_id: ActorId,
    amount: i32,
    at_tiles: Vec<GridPos>,
}

impl Event for LoseLifeEvent {
    type Children = ();

    fn apply(self: Box<Self>, game: &mut GameState) {
        if let Some(actor) = game.actors.get_mut(&self.actor_id) {
            actor.health -= self.amount;
        }
    }

    fn to_log(&self, game: &GameState) -> Option<ColoredText> {
        let actor_name = game.get_actor_or_panic(self.actor_id).colored_name();

        Some(actor_name + format!(" loses {} life.", self.amount))
    }

    fn animate_self(self: Box<Self>) -> GeneralAnimation {
        let Self {
            amount, at_tiles, ..
        } = &*self;
        let text: ColoredText = format!("-{amount}").colored(HP_LOST_TEXT_COLOR);

        let text_start_pos: WorldPoint = barycenter(at_tiles.iter().map(|pos| pos.cast()));

        let flash = flash_tiles(at_tiles.clone(), HP_LOST_TEXT_COLOR);
        GeneralAnimation::dummy(self)
            .with_child(floating_text(text, text_start_pos))
            .with_child(flash)
    }
}

pub fn lose_life(
    b: &mut BackSide,
    actor_id: ActorId,
    amount: i32,
) -> BoxedEventNode<LoseLifeEvent> {
    let pos = b
        .game
        .get_actor_or_panic(actor_id)
        .occupied_tiles()
        .collect();
    let event = LoseLifeEvent {
        actor_id,
        amount,
        at_tiles: pos,
    };
    b.execute_leaf_event(event)
}
