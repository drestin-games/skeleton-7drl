use crate::{
    constants::{HIT_DURATION, HIT_MOVE_RATIO},
    gameplay::{
        back::back::BackSide,
        front::animation::animation::{Flags, GeneralAnimation},
        state::game_state::GameState,
    },
    ui::colored_text::{ColoredText, ColoredTextIter},
    utils::{
        animated::{Animated, Easing, Interpolation},
        geometry::Direction,
        id::ActorId,
    },
};

use super::{
    game_event::{BoxedEventNode, Event},
    lose_life_event::{lose_life, LoseLifeEvent},
};

#[derive(Debug, Clone)]
pub struct ActorHitEvent {
    pub attacker_id: ActorId,
    pub targets: Vec<ActorId>,
    pub dir: Direction,
}

impl Event for ActorHitEvent {
    type Children = Vec<LoseLifeEvent>;
    fn to_log(&self, game: &GameState) -> Option<ColoredText> {
        let attacker_name = game.get_actor_or_panic(self.attacker_id).colored_name();
        let mut text = attacker_name;
        text += " hits ";
        text += self
            .targets
            .iter()
            .map(|id| game.get_actor_or_panic(*id).colored_name())
            .join(", ".into());
        text += ".";

        Some(text)
    }

    fn animate_self(self: Box<Self>) -> GeneralAnimation {
        let Self {
            attacker_id, dir, ..
        } = *self;
        let half_duration = HIT_DURATION / 2;
        let animated_ratio = Interpolation::zero_one(half_duration, Easing::Linear).chain_rev();
        let anim = GeneralAnimation::simple(self, animated_ratio.duration(), move |p| {
            let attacker = p.game.actors.get(&attacker_id);

            if let Some(attacker) = attacker {
                let pos = attacker.pos.cast()
                    + dir.delta_vec().cast() * animated_ratio.get(p.elapsed) * HIT_MOVE_RATIO;
                p.overrides.overriden_positions.insert(attacker_id, pos);
            }
        })
        .with_children_blocking_duration(half_duration)
        .with_flags(Flags::MOVE)
        .with_incompatible_flags(Flags::MOVE);

        anim
    }
}

pub fn actor_hit(
    b: &mut BackSide,
    attacker_id: ActorId,
    targets: Vec<ActorId>,
    dir: Direction,
) -> BoxedEventNode<ActorHitEvent> {
    let event = ActorHitEvent {
        attacker_id,
        targets: targets.clone(),
        dir,
    };
    b.execute_event_with_consequences(event, |b| {
        targets
            .into_iter()
            .map(|id| lose_life(b, id, 2))
            .collect::<Vec<_>>()
    })
}
