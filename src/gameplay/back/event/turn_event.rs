use crate::{
    gameplay::back::{back::BackSide, event::game_event::Event},
    utils::{
        algorithms::pathfinding::find_shortest_path,
        geometry::{Direction, GridPosExtra},
        id::ActorId,
        rng::Choosable,
        shapes::rectangle,
    },
};

use super::{actor_execute_action_event::actor_execute_action, game_event::BoxedEventNode};

#[derive(Clone, Debug)]
pub struct TurnEvent;

impl Event for TurnEvent {}

pub fn handle_move_input(b: &mut BackSide, dir: Direction) -> Option<BoxedEventNode<TurnEvent>> {
    let player_action = b.game.action_from_dir_input(b.game.get_player(), dir);

    if let Some(action) = player_action {
        Some(b.execute_event_with_consequences(TurnEvent, |b| {
            let mut res = vec![actor_execute_action(b, b.game.get_player().id, action, dir)];

            let actor_ids: Vec<ActorId> = b
                .game
                .actors
                .keys()
                .filter(|&&id| !b.game.is_player(id))
                .copied()
                .collect();

            for actor_id in actor_ids {
                let actor = b.game.get_actor_or_panic(actor_id);
                let player = b.game.get_player();
                let is_goal = |pos| {
                    let new_rect = actor.rect_at_pos(pos);
                    player.rect().intersects(&new_rect)
                };
                let is_walkable = |pos| {
                    let new_rect = actor.rect_at_pos(pos);
                    rectangle(new_rect).all(|pos| {
                        b.game.tile_at(pos).is_walkable()
                            && b.game
                                .actors
                                .values()
                                .all(|a| a.id == actor_id || !a.rect().intersects(&new_rect))
                    })
                };
                let action_opt = find_shortest_path(actor.pos, player.pos, &is_goal, &is_walkable)
                    .and_then(|path| {
                        let new_pos = *path.front().expect("Error: empty path returned");
                        let dir = actor.pos.try_dir(new_pos).expect("Should be a valid dir");
                        b.game.action_from_dir_input(actor, dir).map(|a| (a, dir))
                    })
                    .or_else(|| {
                        Direction::ALL_DIR_8
                            .iter()
                            .filter_map(|&dir| {
                                b.game.action_from_dir_input(actor, dir).map(|a| (a, dir))
                            })
                            .choose(&mut b.server_data.rng)
                    });
                if let Some((action, dir)) = action_opt {
                    res.push(actor_execute_action(b, actor_id, action, dir));
                }
            }

            res
        }))
    } else {
        None
    }
}
