use crate::{
    gameplay::{back::back::BackSide, state::game_state::GameState},
    utils::geometry::GridPos,
};

use super::{
    change_level_event::{change_level, ChangeLevelEvent},
    game_event::{BoxedEventNode, Event},
};

#[derive(Debug, Clone)]
pub struct DescendStairsEvent;

impl Event for DescendStairsEvent {
    type Children = ChangeLevelEvent;
}

pub fn descend_stairs(b: &mut BackSide) -> BoxedEventNode<DescendStairsEvent> {
    b.execute_event_with_consequences(DescendStairsEvent, |b| {
        let mut player = b.game.get_player().clone();
        player.pos = GridPos::new(6, 1);
        let new_game_state = GameState::new(player, &mut b.server_data);

        let child = change_level(b, new_game_state);
        child
    })
}
