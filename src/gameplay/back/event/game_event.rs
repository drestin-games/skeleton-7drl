use std::fmt::Debug;

use crate::{
    gameplay::{
        front::animation::animation::{BoxedAnimation, GeneralAnimation},
        state::game_state::GameState,
    },
    ui::colored_text::ColoredText,
    utils::rank::Rank,
};

pub type BoxedAnyEvent = Box<dyn AnyEvent>;
// pub type BoxedEvent<E> = Box<E>;
pub type BoxedChildrenOf<E> = <<E as Event>::Children as EventChildren>::Boxed;
// pub type BoxedChildren<C> = <C as EventChildren>::Boxed;
pub type BoxedAnyEventNode = Box<dyn AnyEventNode>;
pub type BoxedEventNode<E> = Box<EventNode<E>>;
pub type UnknownEventChildren = Vec<BoxedAnyEventNode>;

/// Events representing the atomic modifications of the game state.
/// Each little thing is represented by a different event.
/// Ex: An actor moves on trap and activates it.
/// One representation could be: Move, ActivateTrap, LoseLife.
/// GameState modifications should only happen inside AtomicGameEvent::apply()
///
/// They are created server-side, and sent to the front where they can
/// be applied to update the rendered game state.
pub trait Event: Debug + Clone {
    type Children: EventChildren = UnknownEventChildren;

    fn apply(self: Box<Self>, _game: &mut GameState) {}
    fn to_log(&self, _game: &GameState) -> Option<ColoredText> {
        None
    }
    fn animate_self(self: Box<Self>) -> GeneralAnimation
    where
        Self: 'static,
    {
        GeneralAnimation::dummy(self)
    }
    fn animate_tree(self: Box<Self>, children: BoxedChildrenOf<Self>, rank: Rank) -> BoxedAnimation
    where
        Self: 'static,
    {
        let children: UnknownEventChildren = Self::Children::into_default(children);
        let mut anim = self.animate_self().with_rank(rank);
        for child_event in children {
            let child_anim = child_event.animate();
            anim.add_child(child_anim);
        }

        Box::new(anim)
    }
}

pub trait AnyEvent: Debug {
    fn apply(self: Box<Self>, game: &mut GameState);
    fn to_log(&self, game: &GameState) -> Option<ColoredText>;
}

impl<E: Event> AnyEvent for E {
    fn apply(self: Box<Self>, game: &mut GameState) {
        self.apply(game);
    }
    fn to_log(&self, game: &GameState) -> Option<ColoredText> {
        self.to_log(game)
    }
}

#[derive(Debug)]
pub struct RankedEvent {
    pub rank: Rank,
    pub event: BoxedAnyEvent,
}

#[derive(Debug)]
#[must_use]
pub struct EventNode<E: Event> {
    rank: Rank,
    event: Box<E>,
    children: BoxedChildrenOf<E>,
}

impl<E: Event + 'static> EventNode<E> {
    pub fn new(rank: Rank, event: Box<E>, children: BoxedChildrenOf<E>) -> Self {
        Self {
            rank,
            event,
            children,
        }
    }

    pub fn _erase_type(self: Box<Self>) -> BoxedAnyEventNode {
        self
    }
}

#[must_use]
pub trait AnyEventNode: Debug /* + AnyEventNodeClone */ {
    fn animate(self: Box<Self>) -> BoxedAnimation;
}

impl<E> AnyEventNode for EventNode<E>
where
    E: Event + 'static,
{
    fn animate(self: Box<Self>) -> BoxedAnimation {
        self.event.animate_tree(self.children, self.rank)
    }
}

/// Workaround to implement Clone on a trait object
/// https://stackoverflow.com/a/30353928/5136460
// pub trait AnyEventNodeClone {
//     fn clone_box(&self) -> Box<dyn AnyEventNode>;
// }

// impl<T> AnyEventNodeClone for T
// where
//     T: 'static + AnyEventNode + Clone,
// {
//     fn clone_box(&self) -> Box<dyn AnyEventNode> {
//         Box::new(self.clone())
//     }
// }

// impl Clone for Box<dyn AnyEventNode> {
//     fn clone(&self) -> Self {
//         self.clone_box()
//     }
// }

pub trait EventChildren {
    type Boxed: Debug;
    fn into_default(children: Self::Boxed) -> UnknownEventChildren;
}

impl EventChildren for UnknownEventChildren {
    type Boxed = Self;
    fn into_default(children: Self::Boxed) -> UnknownEventChildren {
        children
    }
}

impl EventChildren for () {
    type Boxed = ();
    fn into_default(_: Self::Boxed) -> UnknownEventChildren {
        Vec::new()
    }
}

impl<E> EventChildren for E
where
    E: Event + 'static,
{
    type Boxed = Box<EventNode<Self>>;
    fn into_default(child: Self::Boxed) -> UnknownEventChildren {
        vec![child]
    }
}

impl<E> EventChildren for Vec<E>
where
    E: EventChildren,
{
    type Boxed = Vec<E::Boxed>;

    fn into_default(children: Self::Boxed) -> UnknownEventChildren {
        children
            .into_iter()
            .flat_map(|c| E::into_default(c))
            .collect()
    }
}

impl<E> EventChildren for Option<E>
where
    E: EventChildren,
{
    type Boxed = Option<E::Boxed>;

    fn into_default(child: Self::Boxed) -> UnknownEventChildren {
        match child {
            Some(child) => E::into_default(child),
            None => Vec::new(),
        }
    }
}

impl<E1, E2> EventChildren for (E1, E2)
where
    E1: EventChildren,
    E2: EventChildren,
{
    type Boxed = (E1::Boxed, E2::Boxed);

    fn into_default((c1, c2): Self::Boxed) -> UnknownEventChildren {
        let mut res = E1::into_default(c1);
        res.append(&mut E2::into_default(c2));

        res
    }
}

impl<E1, E2, E3> EventChildren for (E1, E2, E3)
where
    E1: EventChildren,
    E2: EventChildren,
    E3: EventChildren,
{
    type Boxed = (E1::Boxed, E2::Boxed, E3::Boxed);

    fn into_default((c1, c2, c3): Self::Boxed) -> UnknownEventChildren {
        let mut res = E1::into_default(c1);
        res.append(&mut E2::into_default(c2));
        res.append(&mut E3::into_default(c3));

        res
    }
}
