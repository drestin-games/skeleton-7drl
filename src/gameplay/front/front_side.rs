use std::collections::BinaryHeap;

use crate::{
    constants::{
        map_font_for_size, CANVAS_RECT, CANVAS_SIZE, FOV_ACTIVATED, MAP_FONT, MAP_PANEL_SIZE,
        SQUARE_CELL_SIDE, TEXT_FONT,
    },
    gameplay::{
        back::event::game_event::{BoxedAnyEvent, BoxedAnyEventNode},
        command::command::Command,
        state::game_state::{Actor, GameState},
    },
    input::InputEvent,
    js_bindings::Env,
    ui::{
        color::Color,
        graphics::{CornerStyle, DashStyle, Graphics},
        log::GameLogs,
        panel::{map_panel::MapPanel, panel::Panel, panel_writer::PanelWriter},
    },
    utils::{
        geometry::{
            Align, GridPos, HAlign, RectExtra, ScreenPoint, ScreenRect, WorldPoint, WorldVec,
        },
        rank::Rank,
    },
};

use super::{
    animation::{manager::AnimationsManager, overrides::AnimationOverrides},
    ui::ui_state::{BoxedUiState, HandleInputParams, Idle},
};

pub struct FrontSide {
    game: GameState,
    last_applied_event_rank: Option<Rank>,
    event_queue: BinaryHeap<EventQueueElem>,
    animation_state: AnimationsManager,
    targetted_tile: Option<GridPos>,
    logs: GameLogs,
    ui_state: BoxedUiState,
    outer_panel: Panel,
    map_panel: MapPanel,
    right_panel: Panel,
    bottom_left_panel: Panel,
}

impl From<GameState> for FrontSide {
    fn from(game: GameState) -> Self {
        let animation_state = AnimationsManager::default();

        let outer_panel =
            Panel::from(ScreenRect::from_size(CANVAS_SIZE)).with_margin(SQUARE_CELL_SIDE / 2.0);

        let (left, right) = outer_panel.border_rect().hsplit(MAP_PANEL_SIZE.width);
        let (top_left, bottom_left) = left.vsplit(MAP_PANEL_SIZE.height);

        let map_panel = MapPanel::new(top_left, WorldPoint::zero());
        let right_panel = Panel::new(right);
        let bottom_left_panel = Panel::new(bottom_left);

        Self {
            game,
            last_applied_event_rank: None,
            event_queue: BinaryHeap::new(),
            animation_state,
            ui_state: Box::new(Idle),
            logs: GameLogs::default(),
            targetted_tile: None,
            outer_panel,
            map_panel,
            right_panel,
            bottom_left_panel,
        }
    }
}

impl FrontSide {
    pub fn update(&mut self, env: &Env) {
        self.targetted_tile = self.screen_to_grid(env.mouse_pos);

        let events = self.animation_state.update(env, &mut self.logs, &self.game);
        for (event, rank) in events {
            let elem = EventQueueElem { rank, event };
            self.event_queue.push(elem);
        }
        loop {
            let pop_event = match (self.last_applied_event_rank, self.event_queue.peek()) {
                (None, Some(_)) => true,
                (Some(current), Some(elem)) if elem.rank.is_next_of(current) => true,
                _ => false,
            };
            if pop_event {
                let elem = self.event_queue.pop().unwrap();
                elem.event.apply(&mut self.game);
                self.last_applied_event_rank = Some(elem.rank);
            } else {
                break;
            }
        }
    }

    pub fn push_game_events(&mut self, env: &Env, root_event: BoxedAnyEventNode) {
        self.animation_state.animate_game_events(env, root_event);
    }

    pub fn render(&mut self, env: &Env, g: &mut Graphics) {
        let animations_overrides = self.animation_state.get_overrides(env, &self.game);

        self.map_panel.set_camera_center(
            animations_overrides
                .camera_center
                .unwrap_or(self.game.get_player().pos.cast() + WorldVec::splat(0.5)),
        );

        g.set_corner_style(CornerStyle::Round);
        g.set_dash_style(DashStyle::CURSOR_LIKE.with_offset(-env.time.as_secs_f64() * 100.0));
        let area = vec![
            GridPos::new(0, 0),
            GridPos::new(1, 1),
            GridPos::new(2, 0),
            GridPos::new(1, -1),
        ]
        .into_iter()
        .collect();
        self.map_panel
            .stroke_perimeter_of_area(g, &area, Color::RED);
        g.set_dash_style(DashStyle::Solid);

        g.set_font(&MAP_FONT);
        for (pos, tile) in self
            .map_panel
            .tiles_in_panel()
            .map(|pos| (pos, self.game.tile_at(pos)))
        {
            if let Some(color) = animations_overrides.tiles_color.get(&pos) {
                self.map_panel.fill_map_tile(g, pos, *color);
            }
            if FOV_ACTIVATED && !self.game.player_vision.is_visible(pos) {
                continue;
            }

            self.map_panel
                .write_map_char(g, tile.symbol(), pos.cast(), tile.color());
        }

        // g.write_multicolor_char(
        //     'A',
        //     self.map_panel.grid_to_screen(GridPos::new(10, 7)),
        //     &[
        //         Color::RED,
        //         Color::BLUE,
        //         Color::RED,
        //         Color::BLUE,
        //         Color::RED,
        //         Color::BLUE,
        //         Color::RED,
        //         Color::BLUE,
        //     ],
        // );

        g.set_opacity(0.2);
        self.map_panel.fill_area(g, &area, Color::RED);
        g.set_opacity(1.0);

        for actor in self
            .game
            .actors
            .values()
            .filter(|a| !FOV_ACTIVATED || self.game.player_vision.is_visible_rect(a.rect()))
        {
            let rendered_pos = self.actor_rendered_pos(&animations_overrides, actor);

            if actor.size >= 2 {
                g.set_font(map_font_for_size(actor.size));
            }
            self.map_panel
                .write_map_char(g, actor.symbol, rendered_pos, actor.color);
            if actor.size >= 2 {
                g.set_font(&MAP_FONT);
            }
        }

        if let Some(mouse_pos) = self.targetted_tile {
            self.map_panel.draw_cursor(g, mouse_pos.cast());
        }

        for extra_text in &animations_overrides.extra_world_texts {
            self.map_panel.write_world_text(g, extra_text);
        }

        g.set_font(&TEXT_FONT);
        g.set_corner_style(CornerStyle::Square);

        self.outer_panel.clear_padding_and_margin(g);
        self.map_panel.panel().clear_padding_and_margin(g);
        self.bottom_left_panel.clear_padding_and_margin(g);
        self.right_panel.clear_padding_and_margin(g);

        self.right_panel.render_border(g);
        self.bottom_left_panel.render_border(g);
        self.map_panel.panel().render_double_border(g);
        self.outer_panel.render_border(g);

        let mut panel_writer = PanelWriter::new(
            self.right_panel.content_rect(),
            Align::MIDDLE_CENTER,
            &TEXT_FONT,
        )
        .from_bottom();
        panel_writer.writeln( "Test2\nTest2\nTest2\nTest2\nTest2\nTest2\nTest2\nTest2\nTest2\nTest2\nTest2\nTest2\nTest2".into());
        panel_writer.writeln("Test".into());
        panel_writer.writeln("Test".into());
        panel_writer.writeln("Test".into());
        panel_writer.writeln("Test".into());
        panel_writer.writeln("Test".into());
        panel_writer.newline();
        panel_writer.write_short_floating("Youhou".into(), HAlign::Right);
        panel_writer.writeln("Test".into());
        panel_writer.render(g);

        self.ui_state.extra_render(g);

        if let Some((color, opacity)) = animations_overrides.screen_fade {
            g.set_opacity(opacity);
            g.fill_rect(CANVAS_RECT, color);
            g.set_opacity(1.0);
        }
    }

    fn actor_rendered_pos(&self, animations: &AnimationOverrides, actor: &Actor) -> WorldPoint {
        animations
            .overriden_positions
            .get(&actor.id)
            .copied()
            .unwrap_or(actor.pos.cast())
    }

    fn screen_to_grid(&self, point: ScreenPoint) -> Option<GridPos> {
        self.map_panel.screen_to_grid(point)
    }

    pub fn handle_input(&mut self, env: &Env, event: InputEvent) -> Option<Command> {
        if self.animation_state.is_blocking_animation_running() {
            return None;
        }

        let params = HandleInputParams {
            event,
            env,
            game: &self.game,
        };
        let result = self.ui_state.handle_input(params);

        if let Some(ui_state) = result.new_ui_state {
            self.ui_state = ui_state;
        }

        result.command
    }
}

struct EventQueueElem {
    rank: Rank,
    event: BoxedAnyEvent,
}

impl Eq for EventQueueElem {}

impl PartialEq for EventQueueElem {
    fn eq(&self, other: &Self) -> bool {
        self.rank == other.rank
    }
}

impl PartialOrd for EventQueueElem {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

// reverse order for the queue to be a min-heap
impl Ord for EventQueueElem {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.rank.cmp(&other.rank).reverse()
    }
}
