use std::collections::{BTreeMap, HashMap};

use crate::{
    ui::{color::Color, panel::map_panel::WorldText},
    utils::{
        geometry::{GridPos, WorldPoint},
        id::ActorId,
    },
};

#[derive(Debug, Default)]
pub struct AnimationOverrides {
    pub camera_center: Option<WorldPoint>,
    pub overriden_positions: BTreeMap<ActorId, WorldPoint>,
    pub tiles_color: HashMap<GridPos, Color>,
    pub screen_fade: Option<(Color, f64)>,
    pub extra_world_texts: Vec<WorldText>,
}
