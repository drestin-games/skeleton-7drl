use std::{fmt::Debug, time::Duration};

use bitflags::bitflags;

use crate::{
    constants::{CHAR_FLASH_DURATION, FLOATING_TEXT_LIFETIME, FLOATING_TEXT_SPEED, TEXT_FONT},
    gameplay::{back::event::game_event::BoxedAnyEvent, state::game_state::GameState},
    ui::{color::Color, colored_text::ColoredText, panel::map_panel::WorldText},
    utils::{
        geometry::{Align, GridPos, WorldPoint},
        rank::Rank,
    },
};

use super::overrides::AnimationOverrides;

pub type AnimFunction = Box<dyn FnMut(ApplyParams) -> ()>;

// Only used to implement Debug
struct AnimFunctionWrapper(AnimFunction);

impl Debug for AnimFunctionWrapper {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        "<AnimFunction>".fmt(f)
    }
}

pub struct UpdateParams<'a> {
    pub elapsed: Duration,
    pub game: &'a GameState,
}

pub struct ApplyParams<'a> {
    pub elapsed: Duration,
    pub overrides: &'a mut AnimationOverrides,
    pub game: &'a GameState,
}

pub type BoxedAnimation = Box<dyn Animation>;
pub trait Animation: Debug {
    fn rank(&self) -> Option<Rank>;
    fn debug_event(&self) -> Option<&BoxedAnyEvent>;
    fn flags(&self) -> Flags;
    fn incompatible_flags(&self) -> Flags;
    fn is_blocking_input(&self) -> bool {
        true
    }
    fn update(&mut self, p: UpdateParams) -> Vec<AnimationAction>;
    fn apply(&mut self, p: ApplyParams);
    fn conflicts_with(&self, other: &BoxedAnimation) -> bool {
        self.flags().intersects(other.incompatible_flags())
            || self.incompatible_flags().intersects(other.flags())
    }
    fn _delayed(self: Self, delay: Duration) -> Delayed<Self>
    where
        Self: Sized,
    {
        Delayed { delay, anim: self }
    }
    fn boxed(self) -> BoxedAnimation
    where
        Self: Sized + 'static,
    {
        Box::new(self)
    }
}

#[derive(Debug)]
pub struct GeneralAnimation {
    rank: Option<Rank>,
    event: Option<BoxedAnyEvent>,
    duration: Duration,
    children_blocking_duration: Duration,
    apply_event_at: Duration,
    flags: Flags,
    incompatible_flags: Flags,
    function: AnimFunctionWrapper,
    children: Vec<BoxedAnimation>,
    blocking_input: bool,
}

impl GeneralAnimation {
    pub fn dummy(event: BoxedAnyEvent) -> Self {
        Self::simple(event, Duration::ZERO, |_| ())
    }

    pub fn simple(
        event: BoxedAnyEvent,
        duration: Duration,
        f: impl FnMut(ApplyParams) -> () + 'static,
    ) -> Self {
        Self {
            rank: None,
            event: Some(event),
            duration,
            children_blocking_duration: duration,
            apply_event_at: duration,
            flags: Flags::empty(),
            incompatible_flags: Flags::empty(),
            function: AnimFunctionWrapper(Box::new(f)),
            children: Vec::new(),
            blocking_input: true,
        }
    }

    pub fn detached(duration: Duration, f: impl FnMut(ApplyParams) -> () + 'static) -> Self {
        Self {
            rank: None,
            event: None,
            duration,
            children_blocking_duration: Duration::ZERO,
            apply_event_at: Duration::ZERO,
            flags: Flags::empty(),
            incompatible_flags: Flags::empty(),
            function: AnimFunctionWrapper(Box::new(f)),
            children: Vec::new(),
            blocking_input: false,
        }
    }

    pub fn with_flags(mut self, flags: Flags) -> Self {
        self.flags = flags;
        self
    }

    pub fn with_incompatible_flags(mut self, incompatible_flags: Flags) -> Self {
        self.incompatible_flags = incompatible_flags;
        self
    }

    pub fn with_children_blocking_duration(mut self, duration: Duration) -> Self {
        self.children_blocking_duration = duration;
        self
    }

    pub fn with_apply_event_at(mut self, elapsed: Duration) -> Self {
        self.apply_event_at = elapsed;
        self
    }

    pub fn with_rank(mut self, rank: Rank) -> Self {
        self.rank = Some(rank);
        self
    }

    pub fn add_child(&mut self, child: BoxedAnimation) -> &mut Self {
        self.children.push(child);
        self
    }

    pub fn with_child(mut self, child: BoxedAnimation) -> Self {
        self.add_child(child);
        self
    }
}

impl Animation for GeneralAnimation {
    fn rank(&self) -> Option<Rank> {
        self.rank
    }

    fn debug_event(&self) -> Option<&BoxedAnyEvent> {
        self.event.as_ref()
    }

    fn flags(&self) -> Flags {
        self.children.iter().fold(self.flags, |f, c| f | c.flags())
    }

    fn incompatible_flags(&self) -> Flags {
        self.children
            .iter()
            .fold(self.incompatible_flags, |f, c| f | c.incompatible_flags())
    }

    fn is_blocking_input(&self) -> bool {
        self.blocking_input
    }

    fn update(&mut self, p: UpdateParams) -> Vec<AnimationAction> {
        let mut actions = Vec::new();
        if p.elapsed >= self.apply_event_at {
            if let Some(event) = self.event.take() {
                actions.push(AnimationAction::ApplyEvent(event));
            }
        }
        if p.elapsed >= self.children_blocking_duration && !self.children.is_empty() {
            let children = std::mem::take(&mut self.children);
            actions.push(AnimationAction::FireAnimations(children));
        }
        if p.elapsed >= self.duration {
            actions.push(AnimationAction::Finished);
        }
        actions
    }

    fn apply(&mut self, p: ApplyParams) {
        (self.function.0)(p);
    }
}

bitflags! {
    pub struct Flags: u8 {
        const MOVE = 0b0001;
    }
}

#[derive(Debug)]
pub enum AnimationAction {
    ApplyEvent(BoxedAnyEvent),
    FireAnimations(Vec<BoxedAnimation>),
    Finished,
}

#[derive(Debug)]
pub struct Delayed<A> {
    delay: Duration,
    anim: A,
}

impl<A: Animation> Animation for Delayed<A> {
    fn rank(&self) -> Option<Rank> {
        self.anim.rank()
    }

    fn debug_event(&self) -> Option<&BoxedAnyEvent> {
        self.anim.debug_event()
    }

    fn flags(&self) -> Flags {
        self.anim.flags()
    }

    fn incompatible_flags(&self) -> Flags {
        self.anim.incompatible_flags()
    }

    fn update(&mut self, mut p: UpdateParams) -> Vec<AnimationAction> {
        p.elapsed = p.elapsed.saturating_sub(self.delay);
        self.anim.update(p)
    }

    fn apply(&mut self, mut p: ApplyParams) {
        p.elapsed = p.elapsed.saturating_sub(self.delay);
        self.anim.apply(p);
    }
}

#[derive(Debug)]
pub struct AnimationGroup {
    anims: Vec<BoxedAnimation>,
}

impl AnimationGroup {
    pub fn _new(anims: impl IntoIterator<Item = BoxedAnimation>) -> Self {
        Self {
            anims: anims.into_iter().collect(),
        }
    }
}

impl Animation for AnimationGroup {
    fn rank(&self) -> Option<Rank> {
        self.anims.iter().filter_map(|a| a.rank()).min()
    }

    fn debug_event(&self) -> Option<&BoxedAnyEvent> {
        self.anims.iter().find_map(|a| a.debug_event())
    }

    fn flags(&self) -> Flags {
        self.anims.iter().fold(Flags::empty(), |f, c| f | c.flags())
    }

    fn incompatible_flags(&self) -> Flags {
        self.anims
            .iter()
            .fold(Flags::empty(), |f, c| f | c.incompatible_flags())
    }

    fn update(&mut self, _p: UpdateParams) -> Vec<AnimationAction> {
        let anims = std::mem::take(&mut self.anims);

        vec![
            AnimationAction::FireAnimations(anims),
            AnimationAction::Finished,
        ]
    }

    fn apply(&mut self, _p: ApplyParams) {}
}

pub fn _flash_tile(tile: GridPos, color: Color) -> BoxedAnimation {
    flash_tiles(vec![tile], color)
}

pub fn flash_tiles(tiles: Vec<GridPos>, color: Color) -> BoxedAnimation {
    let f: AnimFunction = Box::new(move |p| {
        if p.elapsed < CHAR_FLASH_DURATION {
            tiles.iter().for_each(|pos| {
                p.overrides.tiles_color.insert(*pos, color);
            });
        }
    });
    Box::new(GeneralAnimation::detached(CHAR_FLASH_DURATION, f)).boxed()
}

pub fn floating_text(text: ColoredText, start_pos: WorldPoint) -> BoxedAnimation {
    let f: AnimFunction = Box::new(move |p| {
        let pos = start_pos + FLOATING_TEXT_SPEED * p.elapsed.as_secs_f64();
        let extra_text = WorldText {
            align: Align::MIDDLE_CENTER,
            font: &TEXT_FONT,
            pos,
            text: text.clone(),
        };
        p.overrides.extra_world_texts.push(extra_text);
    });
    Box::new(GeneralAnimation::detached(FLOATING_TEXT_LIFETIME, f)).boxed()
}
