use crate::{
    console_log,
    constants::DEBUG_ANIMATIONS,
    gameplay::{
        back::event::game_event::{BoxedAnyEvent, BoxedAnyEventNode},
        state::game_state::GameState,
    },
    js_bindings::Env,
    ui::log::GameLogs,
    utils::{rank::Rank, rust::DebugFunction, time::Time},
};
use std::{collections::VecDeque, time::Duration};

use super::{
    animation::{AnimationAction, ApplyParams, BoxedAnimation, UpdateParams},
    overrides::AnimationOverrides,
};

#[derive(Default)]
pub struct AnimationsManager {
    running_animations: VecDeque<(BoxedAnimation, Time)>,
    waiting_animations: VecDeque<BoxedAnimation>,
}

// todo children of A should not block children of B in queue, if they are compatible (I think?)
impl AnimationsManager {
    pub fn update(
        &mut self,
        env: &Env,
        logs: &mut GameLogs,
        game: &GameState,
    ) -> Vec<(BoxedAnyEvent, Rank)> {
        let mut output_events = Vec::new();

        let mut indexes_to_remove = Vec::new();

        let mut changed = false;
        for (i, (anim, start_time)) in self.running_animations.iter_mut().enumerate() {
            let elapsed = env.time - *start_time;
            let (keep, this_changed) = process_animation(
                anim,
                elapsed,
                &mut output_events,
                &mut self.waiting_animations,
                game,
            );

            if !keep {
                indexes_to_remove.push(i);
            }

            changed |= this_changed;
        }

        // indexes_to_remove is sorted because constructed in ascending order
        for i in indexes_to_remove.into_iter().rev() {
            self.running_animations.remove(i);
        }

        while let Some(mut anim) = self.get_first_startable_animation() {
            let elapsed = Duration::ZERO;
            let (keep, this_changed) = process_animation(
                &mut anim,
                elapsed,
                &mut output_events,
                &mut self.waiting_animations,
                game,
            );

            if keep {
                self.push_running(anim, env.time);
            }

            changed |= this_changed;
        }

        if changed && DEBUG_ANIMATIONS {
            self._debug_animations("changed");
        }

        for (event, _) in output_events.iter() {
            if let Some(log) = event.to_log(game) {
                logs.push(log);
            }
        }
        output_events
    }

    pub fn get_overrides(&mut self, env: &Env, game: &GameState) -> AnimationOverrides {
        let mut overrides = AnimationOverrides::default();
        for (anim, start_time) in self.running_animations.iter_mut() {
            let elapsed = env.time - *start_time;
            let params = ApplyParams {
                overrides: &mut overrides,
                elapsed,
                game,
            };
            anim.apply(params);
        }
        overrides
    }

    pub fn is_blocking_animation_running(&self) -> bool {
        self.running_animations
            .iter()
            .any(|(a, _)| a.is_blocking_input())
    }

    pub fn animate_game_events(&mut self, env: &Env, root_event: BoxedAnyEventNode) {
        self.push_running(root_event.animate(), env.time)
    }

    pub fn push_running(&mut self, anim: BoxedAnimation, time: Time) {
        self.running_animations.push_back((anim, time));
    }

    fn get_first_startable_animation(&mut self) -> Option<BoxedAnimation> {
        if let Some(animation) = self.waiting_animations.front() {
            if !self.is_any_running_conflicting_with(animation) {
                let animation = self.waiting_animations.pop_front().unwrap();
                return Some(animation);
            }
        }

        None
    }

    fn is_any_running_conflicting_with(&self, animation: &BoxedAnimation) -> bool {
        self.running_animations
            .iter()
            .map(|(a, _)| a)
            .any(|a| a.conflicts_with(animation))
    }

    fn _debug_animations(&self, step: &str) {
        console_log!("--- {step} ---");
        self.running_animations.iter().for_each(|(a, _)| {
            a.debug_event().debug(">R>");
        });
        self.waiting_animations.iter().for_each(|a| {
            a.debug_event().debug("W");
        });
    }
}

// This function is outside the impl block because it would need mutable access
// to both the animation and 'self'.
// Instead, it takes as argument &mut references to the field it needs.
fn process_animation(
    anim: &mut BoxedAnimation,
    elapsed: Duration,
    output_events: &mut Vec<(BoxedAnyEvent, Rank)>,
    waiting_animations: &mut VecDeque<BoxedAnimation>,
    game: &GameState,
) -> (bool, bool) {
    let params = UpdateParams { elapsed, game };
    let actions = anim.update(params);

    let mut keep = true;
    let mut changed = false;
    for action in actions {
        use AnimationAction::*;
        match action {
            ApplyEvent(event) => {
                output_events.push((
                    event,
                    anim.rank()
                        .expect("All animations with event should have a rank."),
                ));
            }
            FireAnimations(children) => {
                changed = true;
                waiting_animations.extend(children);
                waiting_animations
                    .make_contiguous()
                    .sort_by(|a, b| a.rank().cmp(&b.rank()));
            }
            Finished => {
                changed = true;
                keep = false;
            }
        }
    }

    (keep, changed)
}
