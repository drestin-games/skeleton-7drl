use std::fmt::Debug;

use crate::{
    gameplay::{command::command::Command, state::game_state::GameState},
    input::InputEvent,
    js_bindings::Env,
    ui::{
        graphics::Graphics,
        panel::popup::{PopupPrompt, YesNoAnswer},
    },
};

pub type BoxedUiState = Box<dyn UiState>;

pub struct HandleInputParams<'a> {
    pub event: InputEvent,
    pub env: &'a Env,
    pub game: &'a GameState,
}

pub trait UiState: Debug {
    fn extra_render(&self, _g: &mut Graphics) {}
    fn handle_input(&mut self, _p: HandleInputParams) -> HandleInputResult {
        HandleInputResult::default()
    }
}

#[derive(Debug, Default)]
pub struct HandleInputResult {
    pub new_ui_state: Option<BoxedUiState>,
    pub command: Option<Command>,
}

#[derive(Debug)]
pub struct Idle;

impl UiState for Idle {
    fn handle_input(&mut self, p: HandleInputParams) -> HandleInputResult {
        use InputEvent::*;
        let mut res = HandleInputResult::default();
        match p.event {
            Arrow(dir) => res.command = Some(Command::Move(dir)),
            DescendStairs if p.game.can_player_descend_stairs() => {
                res.new_ui_state = Some(Box::new(PromptStairs::new()))
            }
            _ => (),
        };

        res
    }
}

#[derive(Debug)]
pub struct PromptStairs {
    popup: PopupPrompt<YesNoAnswer>,
}

impl PromptStairs {
    pub fn new() -> Self {
        let question = "Descend stairs (no coming back)?";
        let popup = PopupPrompt::yes_no(question.into());
        Self { popup }
    }
}

impl UiState for PromptStairs {
    fn extra_render(&self, g: &mut Graphics) {
        self.popup.render(g);
    }

    fn handle_input(&mut self, p: HandleInputParams) -> HandleInputResult {
        let mut res = HandleInputResult::default();

        use InputEvent::*;
        if p.event == Enter || (p.event == Click && self.popup.is_mouse_on_answer(p.env.mouse_pos))
        {
            let answer = self.popup.selected_answer();
            if answer.is_yes() {
                res.command = Some(Command::DescendStairs);
            }
            res.new_ui_state = Some(Box::new(Idle));
        } else if p.event == MouseMove {
            self.popup.select_answer_with_mouse(p.env.mouse_pos);
        } else if let Arrow(dir) = p.event {
            self.popup.change_answer_from_dir(dir);
        }

        res
    }
}
