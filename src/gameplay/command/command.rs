use crate::utils::geometry::Direction;

/// Commands are created by the front-side, for actions
/// that provoke changes to the game-state.
/// They are sent to the server-side, which responds
/// with GameEvents.
#[derive(Debug, Clone)]
pub enum Command {
    Move(Direction),
    DescendStairs,
}
