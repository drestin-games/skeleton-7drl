use crate::{
    input::InputEvent,
    js_bindings::Env,
    screen::{app_state::AppState, splash::SplashScreenState},
    ui::graphics::Graphics,
    utils::{id::IdGenerator, rank::RankGenerator, rng::MyRng},
};

use super::{back::back::BackSide, front::front_side::FrontSide};

pub struct GameplayScreenState {
    back: BackSide,
    front: FrontSide,
}

impl GameplayScreenState {
    pub fn with_seed(seed: u64) -> Self {
        let back = BackSide::with_seed(seed);
        let front = FrontSide::from(back.current_game_state().clone());
        Self { front, back }
    }
}

impl GameplayScreenState {
    pub fn handle_event(mut self, env: &Env, event: InputEvent) -> AppState {
        use InputEvent::*;
        match event {
            Escape => SplashScreenState.into(),
            _ => {
                if let Some(command) = self.front.handle_input(env, event) {
                    if let Some(event) = self.back.handle_command(command) {
                        self.front.push_game_events(env, event);
                    }
                }
                self.into()
            }
        }
    }

    pub fn tick(mut self, env: &Env, g: &mut Graphics) -> AppState {
        self.front.update(env);

        self.front.render(&env, g);

        self.into()
    }
}

#[derive(Debug)]
pub struct ServerOnlyData {
    pub id_generator: IdGenerator,
    pub rng: MyRng,
    pub event_rank_generator: RankGenerator,
}

impl ServerOnlyData {
    pub fn with_rng_seed(seed: u64) -> Self {
        Self {
            id_generator: Default::default(),
            rng: MyRng::from_seed(seed),
            event_rank_generator: RankGenerator::new(1),
        }
    }
}
