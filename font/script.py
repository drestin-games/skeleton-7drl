# This script outputs instructions to draw a snap grid in FontForge.
# Copy these instructions just below the only occurence of 'Grid'
# and before the next 'EndSplineSet' in a .sfd (FontForge file).

GRID_STEP = 64
SNAP_STEP = 32
MIN_X = -256
MAX_X = 1024
MIN_Y = MIN_X
MAX_Y = MAX_X

# Lines
for y in range(MIN_Y, MAX_Y + 1, GRID_STEP):
    print(f"{MIN_X} {y} m 1")
    print(f"{MAX_X} {y} l 1")

# Lines
for x in range(MIN_X, MAX_X + 1, GRID_STEP):
    print(f"{x} {MIN_Y} m 1")
    print(f"{x} {MAX_Y} l 1")

# By moving the pen, we can add snap points without drawing anything
for y in range(MIN_Y, MAX_Y + 1, SNAP_STEP):
    for x in range(MIN_X, MAX_X + 1, SNAP_STEP):
        print(f"{x} {y} m 1")
