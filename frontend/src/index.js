import initWasm, { init_game } from "./lib/lib.js";


const startTime = Date.now();
let time = startTime;

let eventCodes = [];
/** @type {HTMLCanvasElement} */
let canvas = null;

let mouseX = 0;
let mouseY = 0;
let mouseDown = false;

function startLoop() {
  time = Date.now();

  canvas = document.getElementById('canvas');
  window.addEventListener('keydown', keydownEventListener);
  canvas.addEventListener('mousedown', () => { eventCodes.push("MouseDown"); mouseDown = true; });
  canvas.addEventListener('mouseup', () => { eventCodes.push("MouseUp"); mouseDown = false; });
  canvas.addEventListener('mouseenter', () => eventCodes.push("MouseEnter"));
  canvas.addEventListener('mouseleave', () => { eventCodes.push("MouseLeave"); mouseDown = false; });
  canvas.addEventListener('mousemove', (event) => {
    const rect = canvas.getBoundingClientRect();
    mouseX = event.clientX - rect.left;
    mouseY = event.clientY - rect.top;
    eventCodes.push('MouseMove');
  });

  window.requestAnimationFrame(loop);
}

function loop() {
  const random = Math.random();
  const newTime = Date.now();
  const delta = newTime - time;
  time = newTime;

  const eventsToPass = eventCodes;
  eventCodes = [];

  if (window.tick !== null) {
    window.tick(random, delta, time - startTime, mouseX, mouseY, mouseDown, eventsToPass);
    // console.log(`Time taken by tick(): ${Date.now() - newTime}`);
  } else {
    console.warn("Calling tick before it is initalized");
  }

  window.requestAnimationFrame(loop);
}

function keydownEventListener(event) {
  let code = event.code;
  if (code.startsWith("Arrow") || code.startsWith("Key") || code.startsWith("Numpad") || ["Space", "Escape", "Enter", "Period"].includes(code)) {
    event.preventDefault();
    eventCodes.push(code);
  } else {
    console.log(`Not intercepted key: ${event.key}`);
  }
}

async function main() {
  await initWasm();

  startLoop();

  await init_game();
  console.warn("Game exited!");
}

main();
