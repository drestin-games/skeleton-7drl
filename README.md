# Drestin's 7DRL skeleton

A skeleton/mini-engine I intend to use for the 7DRLs.

## Development

```bash
# watch rust files to compile wasm (needs: entr)
# (quick double Ctrl-C to quit)
./run-dev

# To export in export.zip for itch.io
./run-export

# In another shell:
cd frontend
# Start the web server, watching js and wasm.
yarn run dev
```

## TODO list
Features I would like to implement.

Items in **bold** are high-priority. Items between parenthesis are low-priority, and seems hard to implement.

- Algorithms
  - Pathfinding
    - **Dijkstra**
    - ~~**Dijkstra map**~~
    - ~~**A\***~~
    - (Multi-agent A*)
  - ~~**Field-of-view**~~
  - Procedural generation
    - Rooms and corridors
    - Cellular automata
    - Prefabs
    - (Wave Function Collapse)
  - ~~Flood fill~~
  - ~~Find perimeter of an area~~
- Animations
  - ~~**Animation chains**~~ => Enhance animations to support more cases:
    - rocket launcher: several child animations fire at different times
    - card drawing: incompatibility only for a given time (we don't have to wait that the card is at the right place to begin drawing the following one)
  - ~~**Generic animations** (move, hit)~~
  - Particles (explosions, snowing ...)
  - Screen-shake
- Gameplay features
  - ~~(Squared) multi-tile actors, with working pathfinding~~
- Mouse interactions
  - ~~Point a tile~~
  - Select on map
  - Select in list
  - (Select a word in a text)
  - (Drag-and-drop)
- UI
  - **UI-state** (ex: select spell, choose target, inventory, inspect mode)
  - ~~**panels**~~
    - ~~**feature: draw text line per line, without the need to give the exact position**~~
  - **gauges (+animations)**
  - **logs**
    - more compact logs: one log for a batch of events.
  - tooltips, contextual hints
  - ~~auto-resize canvas (to support full-screen on itch.io)~~
  - Color operations (lighten, darken, hue-rotation...)
- AI
  - **Basic AI-controlled actors** (attack the player, stay at a distance, flee, stay in groups)
  - (generic AI controlled by heuristics and a list of possible actions...)
- QoL
  - Move to clicked tile
  - (auto-explore)
- Options screen (toggle flashes and screen-shake, input options, sound volume...)
- Music and Sounds
- Save and load

## Credits

- [Vision algorithm](http://www.adammil.net/blog/v125_Roguelike_Vision_Algorithms.html#mycode)
